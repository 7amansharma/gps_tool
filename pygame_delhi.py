import pygame
import numpy as np
#import time
import geopy
from geopy.distance import VincentyDistance
from datetime import datetime
import os
#from Devices import *
import json
import pandas as pd
from math import *
import time
import traceback
CTRL_KEY=306
LEFT_ARROW_KEY=276
RIGHT_ARROW_KEY=275
UP_ARROW_KEY=273
DOWN_ARROW_KEY=274
W_KEY=119
S_KEY=115
SPACE_BAR_KEY=32
A_KEY=97
M_KEY=109
SHIFT_KEY=304
B_KEY=98
D_KEY=100
DELETE_KEY=127
P_KEY=112
L_KEY=108
K_KEY=107
NUMERIC_PLUS_KEY=270
NUMERIC_MINUS_KEY=269



class Map:
	def __init__(self,display,name="DefaultMap",point_mappings=None,mapImg=None,keyLocations=None,Locations=None):
		self.display=display
		self.name=name
		self.onMapEquipmentList=[]
		self.beaconList=[]
		self.mapImg=mapImg
		self.selected_equipment=None
		self.keyLocations=keyLocations	
		self.Locations=Locations
		if point_mappings is not None and len(point_mappings["1"]) > 2 and len(point_mappings["2"]) > 2 :
			self.point_mappings=point_mappings			
			self.point_mapping1=point_mappings["1"]
			self.point_mapping2=point_mappings["2"]
			self.mapRatio=distanceBetweenPoints(self.point_mapping1[:2],self.point_mapping2[:2])/(distanceBtwCoordinates(self.point_mapping1[2:],self.point_mapping2[2:])/1000.0)
			self.mapAngle=abs(abs(bearing_between_points(self.point_mapping1[:2],self.point_mapping2[:2])) - abs(BearingBtwCoordinates(self.point_mapping1[2:],self.point_mapping2[2:])) )
		else:
			self.point_mapping1=None
			self.point_mapping2=None
			self.mapRatio=10000
			self.point_mappings=None

	def add_equipment(self,equipment):
		self.onMapEquipmentList.append(equipment)
		equipment.map=self
		if self.point_mapping1 is not None:
			equipment.referenceMapping=self.point_mapping1
			equipment.mapRatio=self.mapRatio
			equipment.referenceOrigin=geopy.Point(self.point_mapping1[2],self.point_mapping1[3])

	def update_selected(self,equipment):
		self.selected_equipment=equipment

	def updateMap(self):
		for equipment in self.onMapEquipmentList:
			temp_beacon_list=[]
			#print "here",equipment.devId,self.beaconList
			for beacon in self.beaconList:
				#print "(self.mapRatio*10.0)/1000.0",(self.mapRatio*20.0)/1000.0,equipment.position
				if beacon.isNear(equipment.position,(self.mapRatio*20.0)/1000.0):
					temp_beacon_list.append(beacon)
					if beacon in equipment.attachedBeacons:
						pygame.draw.line(self.display,(0,0,255),equipment.position,[beacon.blitPosition[0],beacon.blitPosition[1]],5)
					else:
						pygame.draw.line(self.display,(255,0,0),equipment.position,[beacon.blitPosition[0],beacon.blitPosition[1]],5)
			equipment.nearBeaconsList=temp_beacon_list
			
	def addBeacons(self,beacon):
		self.beaconList.append(beacon)
		#fprint "self.beaconList",self.beaconList
			#self.display.blit(line,)

	def getNearestBeacon(self,equipmentObject):
		possible_beacons=[beacon for beacon in equipmentObject.nearBeaconsList if beacon not in equipmentObject.attachedBeacons]
		if len(possible_beacons)>0:
			rssi_list=[beacon.getRSSI([equipmentObject.lat2,equipmentObject.lon2],equipmentObject.pixles_to_latlong(beacon.position)) for beacon in possible_beacons]
			nearest_idx=rssi_list.index(min(rssi_list))
			return possible_beacons[nearest_idx]
		else:
			return None


def text_objects(text, font):
	textSurface = font.render(text, True, (0,0,0))
	return textSurface, textSurface.get_rect()
def message_display(text,position,gameDisplay):
	largeText = pygame.font.Font('freesansbold.ttf',20)
	TextSurf, TextRect = text_objects(text, largeText)
	TextRect.center = position
	gameDisplay.blit(TextSurf, TextRect)
	#print "here"


def distanceBtwCoordinates(loc1,loc2):
	try:
		#utility.loginfo(str(loc1)+" "+str(loc2))
		loc1_lat_radians=radians(float(loc1[0]))
		#print(loc1_lat_radians)
		loc1_long_radians=radians(float(loc1[1]))
		loc2_lat_radians=radians(float(loc2[0]))
		loc2_long_radians=radians(float(loc2[1]))
		diff_lat=loc1_lat_radians - loc2_lat_radians
		diff_long=loc1_long_radians - loc2_long_radians
		intermediate_result=sin(diff_lat/2)*sin(diff_lat/2) + cos(loc1_lat_radians)*cos(loc2_lat_radians)\
		*sin(diff_long/2)*sin(diff_long/2)
		return (6371000*(2*atan2(sqrt(intermediate_result),sqrt(1-intermediate_result))))  + 0.000001
	except Exception as e:
		#utility.loginfo("Exception distanceBtwCoordinates :"+str(e)+str(traceback.format_exc())+" loc1,loc2= "+str(loc1)+" "+str(loc2))
		return 9999	


def latLon_to_xy(mapObject,reference_gps_mapping_array,latlon_tuple):
	x_distance=distanceBtwCoordinates([reference_gps_mapping_array[2],reference_gps_mapping_array[3]],[reference_gps_mapping_array[2],latlon_tuple[1]])/1000.0
	y_distance=distanceBtwCoordinates([reference_gps_mapping_array[2],reference_gps_mapping_array[3]],[latlon_tuple[0],reference_gps_mapping_array[3]])/1000.0
	x_distance=x_distance*mapObject.mapRatio
	y_distance=y_distance*mapObject.mapRatio
	dx=latlon_tuple[1]-reference_gps_mapping_array[3]
	dy=latlon_tuple[0]-reference_gps_mapping_array[2]
	if dx>0:
		x=reference_gps_mapping_array[0]+x_distance
	else:
		x=reference_gps_mapping_array[0]-x_distance
	if dy>0:
		y=reference_gps_mapping_array[1]-y_distance
	else:
		y=reference_gps_mapping_array[1]+y_distance
	return [int(x),int(y)]

def pixles_to_latlong(position,mapObject):
	referenceMapping,referenceOrigin=[mapObject.point_mapping1[0],mapObject.point_mapping1[1]],geopy.Point(mapObject.point_mapping1[2],mapObject.point_mapping1[3])
	d=np.sqrt(np.power(position[0] - referenceMapping[0],2) + np.power(position[1] - referenceMapping[1],2))/(mapObject.mapRatio)
	###print "d",d,self.position[0],self.startPosition[0],(self.position[0] - self.startPosition[0])
	if position[0] == referenceMapping[0]:
		b=0
	else:
		b=90 + np.arctan2((position[1] - referenceMapping[1]),(position[0] - referenceMapping[0]))*(180/np.pi) 
	###print "b",b,90-b
	destination = VincentyDistance(kilometers=d).destination(referenceOrigin, b)
	#self.lat2, self.lon2 = 
	return destination.latitude, destination.longitude


def distanceBetweenPoints(point1,point2):
	return np.sqrt(np.power(point1[0] - point2[0],2) + np.power(point1[1] - point2[1],2))

def BayToSpecificLocation(bay,equipment_loc,deviceId):
    global Bay_Locations
    try:
        direction="NA"
        position="NA"
        #print deviceId
        if "ACOA_" in deviceId:
            perpendicular_distance_threshold=40
            parallel_distance_threshold=10
        elif "APBT_" in deviceId:
            perpendicular_distance_threshold=10
            parallel_distance_threshold=15
        elif "ALAD_" in deviceId:
            perpendicular_distance_threshold=15
            parallel_distance_threshold=5
        elif "ACAT_" in deviceId:
            perpendicular_distance_threshold=15
            parallel_distance_threshold=5
        elif "ABFL_" in deviceId:
            perpendicular_distance_threshold=15
            parallel_distance_threshold=5                        
        else:
            perpendicular_distance_threshold=25
            parallel_distance_threshold=10
        #print deviceId,parallel_distance_threshold,perpendicular_distance_threshold,bay
        #utility.loginfo("BayToSpecificLocation : "+str(bay)+" "+str(equipment_loc))
        if bay in Bay_Locations.keys():
            equipment_bearing=BearingBtwCoordinates(Bay_Locations[bay]["D"],equipment_loc)
            angle_btw_DA_and_Dequipment=(equipment_bearing - Bay_Locations[bay]["BearingDA"]) if (equipment_bearing - Bay_Locations[bay]["BearingDA"]) > 0 else (Bay_Locations[bay]["BearingDA"] - equipment_bearing)
            if Bay_Locations[bay]["BearingDA"] >= -180:
                if (equipment_bearing < Bay_Locations[bay]["BearingDA"] and equipment_bearing > (Bay_Locations[bay]["BearingDA"]-180) ):
                    #true == left
                    angle_btw_DA_and_Dequipment=angle_btw_DA_and_Dequipment
                else:
                    angle_btw_DA_and_Dequipment=-angle_btw_DA_and_Dequipment
            else:
                if (equipment_bearing > Bay_Locations[bay]["BearingDA"] and equipment_bearing < (Bay_Locations[bay]["BearingDA"]+180) ):
                    angle_btw_DA_and_Dequipment=-angle_btw_DA_and_Dequipment
                else:
                    angle_btw_DA_and_Dequipment=angle_btw_DA_and_Dequipment
            distance_D_equipment=distanceBtwCoordinates(equipment_loc,Bay_Locations[bay]["D"])        
            parallel_distance= distance_D_equipment*cos((angle_btw_DA_and_Dequipment*pi)/180.0 )
            perpendicular_distance= distance_D_equipment*sin((angle_btw_DA_and_Dequipment*pi)/180.0 )
            if perpendicular_distance < perpendicular_distance_threshold and perpendicular_distance > -perpendicular_distance_threshold:
                if abs(angle_btw_DA_and_Dequipment) > 90 and abs(angle_btw_DA_and_Dequipment) < (90+180) and parallel_distance > -parallel_distance_threshold and parallel_distance < 0  :
                    position="B"
                elif parallel_distance > Bay_Locations[bay]["DA"] and parallel_distance < (Bay_Locations[bay]["DA"] + parallel_distance_threshold) :
                    position="T"
                elif parallel_distance < Bay_Locations[bay]["DA"] and parallel_distance > Bay_Locations[bay]["DB"]:
                    position="A_B"
                elif parallel_distance < Bay_Locations[bay]["DB"] and parallel_distance > Bay_Locations[bay]["DC"]:
                    position="B_C"
                elif parallel_distance < Bay_Locations[bay]["DC"] and parallel_distance > 0 :
                    position="C_D"
                if position!="NA":
                    #print bay,(equipment_bearing , Bay_Locations[bay]["BearingDA"] , equipment_bearing , (Bay_Locations[bay]["BearingDA"]+180) )
                    if Bay_Locations[bay]["BearingDA"] >= -180:
                        if (equipment_bearing < Bay_Locations[bay]["BearingDA"] and equipment_bearing > (Bay_Locations[bay]["BearingDA"]-180) ):
                            #true == left
                            direction="L"
                        else:
                            direction="R"       
                    else:
                        if (equipment_bearing > Bay_Locations[bay]["BearingDA"] and equipment_bearing < (Bay_Locations[bay]["BearingDA"]+180) ):
                            direction="R"
                        else:
                            direction="L"
                else:
                    return "NA"
            else:
                return "NA"

            return position+"_"+direction
        else:
            if bay in nonBayLocations.keys():
                distance=distanceBtwCoordinates(nonBayLocations[bay]["latlon"].split(","),equipment_loc)
                if distance < 20:
                    return "LOC"
            return "NA"
        pass
    except Exception as e:
        #utility.loginfo("except in BayToSpecificLocation "+str(e))
        return "NA"	
	# global Bay_Locations
	# direction="NA"
	# position="NA"
	# #utility.loginfo("BayToSpecificLocation : "+str(bay)+" "+str(equipment_loc))
	# if bay in Bay_Locations.keys():
	# 	equipment_bearing=BearingBtwCoordinates(Bay_Locations[bay]["D"],equipment_loc)
	# 	angle_btw_DA_and_Dequipment=(equipment_bearing - Bay_Locations[bay]["BearingDA"]) if (equipment_bearing - Bay_Locations[bay]["BearingDA"]) > 0 else (Bay_Locations[bay]["BearingDA"] - equipment_bearing)
	# 	if Bay_Locations[bay]["BearingDA"] >= -180:
	# 		if (equipment_bearing < Bay_Locations[bay]["BearingDA"] and equipment_bearing > (Bay_Locations[bay]["BearingDA"]-180) ):
	# 			#true == left
	# 			angle_btw_DA_and_Dequipment=angle_btw_DA_and_Dequipment
	# 		else:
	# 			angle_btw_DA_and_Dequipment=-angle_btw_DA_and_Dequipment
	# 	else:
	# 		if (equipment_bearing > Bay_Locations[bay]["BearingDA"] and equipment_bearing < (Bay_Locations[bay]["BearingDA"]+180) ):
	# 			angle_btw_DA_and_Dequipment=-angle_btw_DA_and_Dequipment
	# 		else:
	# 			angle_btw_DA_and_Dequipment=angle_btw_DA_and_Dequipment
	# 	distance_D_equipment=distanceBtwCoordinates(equipment_loc,Bay_Locations[bay]["D"])        
	# 	parallel_distance= distance_D_equipment*cos((angle_btw_DA_and_Dequipment*pi)/180.0 )
	# 	perpendicular_distance= distance_D_equipment*sin((angle_btw_DA_and_Dequipment*pi)/180.0 )
	# 	if perpendicular_distance < 30 and perpendicular_distance > -30:
	# 		if abs(angle_btw_DA_and_Dequipment) > 90 and abs(angle_btw_DA_and_Dequipment) < (90+180) and parallel_distance > -20 and parallel_distance < 0  :
	# 			position="B"
	# 		elif parallel_distance > Bay_Locations[bay]["DA"] and parallel_distance < (Bay_Locations[bay]["DA"] + 20) :
	# 			position="T"
	# 		elif parallel_distance < Bay_Locations[bay]["DA"] and parallel_distance > Bay_Locations[bay]["DB"]:
	# 			position="A_B"
	# 		elif parallel_distance < Bay_Locations[bay]["DB"] and parallel_distance > Bay_Locations[bay]["DC"]:
	# 			position="B_C"
	# 		elif parallel_distance < Bay_Locations[bay]["DC"] and parallel_distance > 0 :
	# 			position="C_D"
	# 		if position!="NA":
	# 			#print bay,(equipment_bearing , Bay_Locations[bay]["BearingDA"] , equipment_bearing , (Bay_Locations[bay]["BearingDA"]+180) )
	# 			if Bay_Locations[bay]["BearingDA"] >= -180:
	# 				if (equipment_bearing < Bay_Locations[bay]["BearingDA"] and equipment_bearing > (Bay_Locations[bay]["BearingDA"]-180) ):
	# 					#true == left
	# 					direction="L"
	# 				else:
	# 					direction="R"   	
	# 			else:
	# 				if (equipment_bearing > Bay_Locations[bay]["BearingDA"] and equipment_bearing < (Bay_Locations[bay]["BearingDA"]+180) ):
	# 					direction="R"
	# 				else:
	# 					direction="L"
	# 		else:
	# 			return "NA"
	# 	else:
	# 		return "NA"

	# 	return position+"_"+direction
	# else:
	# 	return "NA"
	# pass

def bearing_between_points(point1,point2):
	x1, y1 = point1[0], point1[1]
	x2, y2 = point2[0], point2[1]
	slope = (y2 - y1)/(x2 - x1)

	slope_angle=degrees(atan(slope))
	if x2 >= x1:
		if (0 <= slope_angle <= 90):
			slope_angle += 270
		elif -90 < slope_angle < 0:
			slope_angle = 270 - abs(slope_angle)
	elif x2 < x1:
		if (0 <= slope_angle <= 90):
			slope_angle = 90 + slope_angle
		elif -90 <= slope_angle < 0:
			slope_angle = 90 + abs(slope_angle)

	return -slope_angle


def BearingBtwCoordinates(loc1,loc2):
	try:
		lat1, lon1, lat2, lon2=map(radians,[float(loc1[0]),float(loc1[1]),float(loc2[0]),float(loc2[1])])
		dLon = lon2 - lon1
		y = sin(dLon) * cos(lat2)
		x = cos(lat1) * sin(lat2) \
			- sin(lat1) * cos(lat2) * cos(dLon)
		Bearing= atan2(y, x)
		Bearing = degrees(Bearing)
		#print Bearing
		Bearing= Bearing if Bearing < 0 else (-180 -(180 - Bearing))    
		return Bearing
	except Exception as e:
		#utility.loginfo("Exception BearingBtwCoordinates :"+str(e)+str(traceback.format_exc()))
		return 0

def distanceFromBay(bay,equipment_loc):
	if bay in Bay_Locations.keys():
		equipment_bearing=BearingBtwCoordinates(Bay_Locations[bay]["D"],equipment_loc)
		angle_btw_DA_and_Dequipment=(equipment_bearing - Bay_Locations[bay]["BearingDA"]) if (equipment_bearing - Bay_Locations[bay]["BearingDA"]) > 0 else (Bay_Locations[bay]["BearingDA"] - equipment_bearing)
		if Bay_Locations[bay]["BearingDA"] >= -180:
			if (equipment_bearing < Bay_Locations[bay]["BearingDA"] and equipment_bearing > (Bay_Locations[bay]["BearingDA"]-180) ):
				#true == left
				angle_btw_DA_and_Dequipment=angle_btw_DA_and_Dequipment
			else:
				angle_btw_DA_and_Dequipment=-angle_btw_DA_and_Dequipment
		else:
			if (equipment_bearing > Bay_Locations[bay]["BearingDA"] and equipment_bearing < (Bay_Locations[bay]["BearingDA"]+180) ):
				angle_btw_DA_and_Dequipment=-angle_btw_DA_and_Dequipment
			else:
				angle_btw_DA_and_Dequipment=angle_btw_DA_and_Dequipment		
		distance_D_equipment=distanceBtwCoordinates(equipment_loc,Bay_Locations[bay]["D"])        
		parallel_distance= distance_D_equipment*cos((angle_btw_DA_and_Dequipment*pi)/180.0 )
		perpendicular_distance= distance_D_equipment*sin((angle_btw_DA_and_Dequipment*pi)/180.0 )
		p_distance=9999
		if parallel_distance > Bay_Locations[bay]["DA"]:
			return parallel_distance - Bay_Locations[bay]["DA"]
		elif parallel_distance < 0:
			return abs(parallel_distance)
		else:
			return abs(perpendicular_distance)
		#return max(perpendicular_distance,p_distance)
	else:
		return 9999
def locationToRefLocation(lat,lon,DevId):
    if lat=="":
        #utility.loginfo("lat , lon are empty = {}, {}".format(lat,lon))
        return ("None",9999)
    try:
        global Locations
        location_map=Locations
        keys=location_map.keys()
        #print keys

        #refLoc=location_map[keys[0]]["Name"]  
        distance_list=[distanceBtwCoordinates([lat,lon],loc.split(",")) for loc in keys]
        sorted_index=sorted(range(len(distance_list)),key=distance_list.__getitem__)
        #min_idx=sorted_index[0]
        #utility.loginfo("distance_list"+str(distance_list)+" min_idx="+str(min_idx)+" bay= "+str(location_map[keys[min_idx]]["Name"])+" justforfun")
        #print [( location_map[keys[i]],distance_list[i]) for i in range(len(keys))  ]
        #print "====="
        #refLoc=location_map[keys[min_idx]]["Name"]
        #if distance_list[min_idx] > 50:
        #   return ("Near_"+refLoc,distance_list[min_idx])
        for idx in sorted_index:
            min_idx=idx
            refLoc=location_map[keys[min_idx]]["Name"]
            equipment_loc=[lat,lon]
            bay=refLoc
            if "ACOA" in DevId:
                if bay in Bay_Locations.keys():
                    equipment_bearing=BearingBtwCoordinates(Bay_Locations[bay]["D"],equipment_loc)
                    angle_btw_DA_and_Dequipment=(equipment_bearing - Bay_Locations[bay]["BearingDA"]) if (equipment_bearing - Bay_Locations[bay]["BearingDA"]) > 0 else (Bay_Locations[bay]["BearingDA"] - equipment_bearing)
                    if Bay_Locations[bay]["BearingDA"] >= -180:
                        if (equipment_bearing < Bay_Locations[bay]["BearingDA"] and equipment_bearing > (Bay_Locations[bay]["BearingDA"]-180) ):
                            #true == left
                            angle_btw_DA_and_Dequipment=angle_btw_DA_and_Dequipment
                        else:
                            angle_btw_DA_and_Dequipment=-angle_btw_DA_and_Dequipment
                    else:
                        if (equipment_bearing > Bay_Locations[bay]["BearingDA"] and equipment_bearing < (Bay_Locations[bay]["BearingDA"]+180) ):
                            angle_btw_DA_and_Dequipment=-angle_btw_DA_and_Dequipment
                        else:
                            angle_btw_DA_and_Dequipment=angle_btw_DA_and_Dequipment
                    if angle_btw_DA_and_Dequipment < 0:
                        continue
                    else:
                        return (refLoc,distanceFromBay(refLoc,[lat,lon]))
                else:
                    return (refLoc,distance_list[min_idx])
            elif ("_AFUEL_" in DevId) or ("_AETT_" in DevId) or ("_ADT_" in DevId):
                if bay in Bay_Locations.keys():
                    equipment_bearing=BearingBtwCoordinates(Bay_Locations[bay]["D"],equipment_loc)
                    angle_btw_DA_and_Dequipment=(equipment_bearing - Bay_Locations[bay]["BearingDA"]) if (equipment_bearing - Bay_Locations[bay]["BearingDA"]) > 0 else (Bay_Locations[bay]["BearingDA"] - equipment_bearing)
                    if Bay_Locations[bay]["BearingDA"] >= -180:
                        if (equipment_bearing < Bay_Locations[bay]["BearingDA"] and equipment_bearing > (Bay_Locations[bay]["BearingDA"]-180) ):
                            #true == left
                            angle_btw_DA_and_Dequipment=angle_btw_DA_and_Dequipment
                        else:
                            angle_btw_DA_and_Dequipment=-angle_btw_DA_and_Dequipment
                    else:
                        if (equipment_bearing > Bay_Locations[bay]["BearingDA"] and equipment_bearing < (Bay_Locations[bay]["BearingDA"]+180) ):
                            angle_btw_DA_and_Dequipment=-angle_btw_DA_and_Dequipment
                        else:
                            angle_btw_DA_and_Dequipment=angle_btw_DA_and_Dequipment
                    if angle_btw_DA_and_Dequipment > 0:
                        continue
                    else:
                        return (refLoc,distanceFromBay(refLoc,[lat,lon]))
                else:
                    return (refLoc,distance_list[min_idx])                
            else:
                if bay in Bay_Locations.keys():
                    return (refLoc,distanceFromBay(refLoc,[lat,lon]))
                else:
                    return (refLoc,distance_list[min_idx])
    except Exception as e:
        #utility.loginfo("error in locationToRefLocation :"+str(e))
        refLoc="None"
        return (refLoc,9999)	

def gpsToGridNo(lat,lon):
    org_lat,org_lon=[17.23333, 78.43096]
    step_dist_meters=10
    x_sign,y_sign=1,1
    if lon<org_lon:
        x_sign=-1
    if lat<org_lat:
        y_sign=-1
    x_dist=distanceBtwCoordinates([org_lat,lon],[org_lat,org_lon])
    y_dist=distanceBtwCoordinates([lat,org_lon],[org_lat,org_lon])
    x=round(x_dist/step_dist_meters)
    y=round(y_dist/step_dist_meters)
    x=int(x*x_sign) if x!=0 else 0
    y=int(y*y_sign) if y!=0 else 0
    return [x,y]


pygame.init()
map_image_path="images/mapDelhi.png"
mapR_image_path="images/mapDelhiR.png"
mapL_image_path="images/mapDelhiL.png"
mapZoomOut1_image_path="images/mapDelhiZoomOut1.png"
current_mapping="Delhi"
map_mappings_name_center="Delhi"
map_mappings_name_right="DelhiR"
map_mappings_name_left="DelhiL"
map_mappings_name_ZoomOut1 = "DelhiZoomOut1"
OperationUnit=22
bays_df=pd.read_csv("AirportLocations.csv")
bays_df.Mid1.dropna(inplace=True)

mapImgR=pygame.image.load(mapR_image_path)
mapImgL=pygame.image.load(mapL_image_path)
mapImg=pygame.image.load(map_image_path)
mapImgZoomOut1=pygame.image.load(mapZoomOut1_image_path)

w,h=mapImg.get_width(),mapImg.get_height()
wR,hR=mapImgR.get_width(),mapImgR.get_height()
wL,hL=mapImgL.get_width(),mapImgL.get_height()
wZO1,hZO1= mapImgZoomOut1.get_width(),mapImgZoomOut1.get_height()

# gameDisplay=pygame.display.set_mode((w,h))
# gameDisplayR=pygame.display.set_mode((wR,hR))
# gameDisplayL=pygame.display.set_mode((wL,hL))
# gameDisplayZoomOut1=pygame.display.set_mode((wZO1,hZO1))

gameDisplay=pygame.display.set_mode((max([w,wR,wL,wZO1]),max([h,hR,hL,hZO1])))

pygame.display.set_caption('ZestIOT')

clock=pygame.time.Clock()

crashed=False

addCarImg=pygame.image.load('images/addCar32x32.png')


black=(0,0,0)
white=(255,255,255)

map_mappings=json.load(open("map_mappings.json","r"))
print map_mappings.keys()

#current_map=Map(gameDisplay,"Hyderabad",point_mappings=map_mappings[current_mapping],mapImg=mapImg)
maps={}

maps["C"]=Map(gameDisplay,map_mappings_name_center,point_mappings=map_mappings[map_mappings_name_center],mapImg=mapImg)
maps["R"]=Map(gameDisplay,map_mappings_name_right,point_mappings=map_mappings[map_mappings_name_right],mapImg=mapImgR)
maps["L"]=Map(gameDisplay,map_mappings_name_left,point_mappings=map_mappings[map_mappings_name_left],mapImg=mapImgL)
maps["ZoomOut1"]=Map(gameDisplay,map_mappings_name_ZoomOut1,point_mappings=map_mappings[map_mappings_name_ZoomOut1],mapImg=mapImgZoomOut1)

current_map=maps["R"]
#print current_map.point_mapping1,current_map.point_mapping2
def strLatLong_to_list_of_tuple(x,current_map,map_mappings):
	try:
		if x is not None:
			#print x,type(x)
			t=x.split(",")
			lat=float(t[0])
			lon=float(t[1])
			
			latLong_tuple=[lat,lon]
			return latLon_to_xy(current_map,map_mappings["4"],latLong_tuple)
		else:
			return None
	except Exception as e:
		print e
		return [0,0]

bays_df=bays_df[bays_df.OperationUnit==OperationUnit]
#print bays_df.Name.tolist()
Locations={}
Bay_Locations={}
for bay in bays_df.Name.unique().tolist():
	tdf=bays_df[bays_df.Name==bay]
	if not pd.isnull(tdf["Mid1"].values[0]):
		Locations[tdf["Mid1"].values[0]]={"Name":bay}
		print tdf["Type"].values[0],tdf["Mid1"].values[0],type(tdf["Mid1"].values[0])
		if tdf["Type"].values[0].lower()=='bay' and not pd.isnull(tdf["Mid1"].values[0]):
			print tdf.values[0],bay
			row=[bay,tdf["Mid1"].values[0],tdf["Mid1"].values[0],tdf["Head"].values[0],tdf["Mid2"].values[0],tdf["Tail"].values[0]]
			Bay_Locations[bay]={"A":row[3].split(","),"B":row[1].split(","),"C":row[4].split(","),"D":row[5].split(",")}
			Bay_Locations[bay]["DC"]=distanceBtwCoordinates(Bay_Locations[row[0]]["D"],Bay_Locations[row[0]]["C"])
			#print bay,"Bay_Locations[bay]['DC']=",Bay_Locations[bay]["DC"]
			Bay_Locations[bay]["DB"]=distanceBtwCoordinates(Bay_Locations[row[0]]["D"],Bay_Locations[row[0]]["B"])
			Bay_Locations[bay]["DA"]=distanceBtwCoordinates(Bay_Locations[row[0]]["D"],Bay_Locations[row[0]]["A"])
			Bay_Locations[bay]["BearingDA"]=BearingBtwCoordinates(Bay_Locations[row[0]]["D"],Bay_Locations[row[0]]["A"])	

print "Bay_Locations>>>>>>>>>>>>>>>",bays_df[bays_df.Name=="15"]	
#print Locations
non_bay_locations=bays_df[bays_df.Type.str.lower()!='bay']
non_bay_locations=non_bay_locations[non_bay_locations.Mid1!=","]
non_bay_locations=non_bay_locations[~non_bay_locations.Mid1.isnull()]
non_bay_locations=non_bay_locations[["Mid1","Name"]]


bays_df=bays_df[bays_df.Type.str.lower()=='bay']
#print map_mappings
name_list=bays_df.Name.tolist()
print "name_list>>>>>>>>>>>>>>>>>",name_list
Mid1=bays_df.Mid1.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["C"],map_mappings[map_mappings_name_center])).tolist()
Mid2=bays_df.Mid2.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["C"],map_mappings[map_mappings_name_center])).tolist()
Head=bays_df.Head.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["C"],map_mappings[map_mappings_name_center])).tolist()
Tail=bays_df.Tail.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["C"],map_mappings[map_mappings_name_center])).tolist()
Locations2=non_bay_locations.Mid1.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["C"],map_mappings[map_mappings_name_center])).tolist()
Locations_names=non_bay_locations.Name.tolist()
maps["C"].Locations=[[Locations2[i],Locations_names[i]] for i in range(len(Locations2))]
maps["C"].keyLocations=[[Head[i],Mid1[i],Mid2[i],Tail[i]] for i in range(len(Head))]


Mid1=bays_df.Mid1.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["R"],map_mappings[map_mappings_name_right])).tolist()
Mid2=bays_df.Mid2.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["R"],map_mappings[map_mappings_name_right])).tolist()
Head=bays_df.Head.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["R"],map_mappings[map_mappings_name_right])).tolist()
Tail=bays_df.Tail.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["R"],map_mappings[map_mappings_name_right])).tolist()
maps["R"].keyLocations=[[Head[i],Mid1[i],Mid2[i],Tail[i]] for i in range(len(Head))]
Locations2=non_bay_locations.Mid1.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["R"],map_mappings[map_mappings_name_right])).tolist()
Locations_names=non_bay_locations.Name.tolist()
maps["R"].Locations=[[Locations2[i],Locations_names[i]] for i in range(len(Locations2))]
#maps["R"].keyLocations=[[Head[i],Mid1[i],Mid2[i],Tail[i]] for i in range(len(Head))]

Mid1=bays_df.Mid1.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["L"],map_mappings[map_mappings_name_left])).tolist()
Mid2=bays_df.Mid2.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["L"],map_mappings[map_mappings_name_left])).tolist()
Head=bays_df.Head.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["L"],map_mappings[map_mappings_name_left])).tolist()
Tail=bays_df.Tail.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["L"],map_mappings[map_mappings_name_left])).tolist()
maps["L"].keyLocations=[[Head[i],Mid1[i],Mid2[i],Tail[i]] for i in range(len(Head))]
#Mid1,Mid2,Head,Tail=None,None,None,None
Locations2=non_bay_locations.Mid1.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["L"],map_mappings[map_mappings_name_left])).tolist()
Locations_names=non_bay_locations.Name.tolist()
maps["L"].Locations=[[Locations2[i],Locations_names[i]] for i in range(len(Locations2))]



Mid1=bays_df.Mid1.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["ZoomOut1"],map_mappings[map_mappings_name_ZoomOut1])).tolist()
Mid2=bays_df.Mid2.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["ZoomOut1"],map_mappings[map_mappings_name_ZoomOut1])).tolist()
Head=bays_df.Head.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["ZoomOut1"],map_mappings[map_mappings_name_ZoomOut1])).tolist()
Tail=bays_df.Tail.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["ZoomOut1"],map_mappings[map_mappings_name_ZoomOut1])).tolist()
maps["ZoomOut1"].keyLocations=[[Head[i],Mid1[i],Mid2[i],Tail[i]] for i in range(len(Head))]
#Mid1,Mid2,Head,Tail=None,None,None,None
Locations2=non_bay_locations.Mid1.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["ZoomOut1"],map_mappings[map_mappings_name_ZoomOut1])).tolist()
Locations_names=non_bay_locations.Name.tolist()
maps["ZoomOut1"].Locations=[[Locations2[i],Locations_names[i]] for i in range(len(Locations2))]
#non_bay_locations,Locations,Locations_names=None,None,None
#display_points=False
#GPSLogs=pd.read_csv("GPSLogs_2018-09-09_00:00:01_2018-09-09_23:59:59.csv")

print ">>>>>>>>a1"
current_samplle_number=0
max_sample_number=11
file_name='GPSLogsDIAL.csv'
GPSLogs,start_time,end_time,equipment_images,unique_devices=None,None,None,None,None
def load_data(sample_number):
	global GPSLogs,start_time,end_time,equipment_images,unique_devices
	GPSLogs=pd.read_csv(file_name)
	print ">>>>>>>>a2"
	GPSLogs=GPSLogs[GPSLogs.LongLat!=","]
	GPSLogs=GPSLogs[GPSLogs.LongLat!=""]
	GPSLogs=GPSLogs[~GPSLogs.DevId.isna()]
	#GPSLogs=GPSLogs[GPSLogs.DevId.str.contains("RGI_ABFL_0004")]
	print len(GPSLogs),GPSLogs.DevId.unique()
	GPSLogs.LogDate=pd.to_datetime(GPSLogs.LogDate,errors='coerce')
	GPSLogs=GPSLogs.sort_values(by="LogDate")		
	print "here1"
	#GPSLogs.drop(columns=['xy_list'])
	if "xy_list" not in GPSLogs.columns or "xy_listZoomOut1"  not in GPSLogs.columns   :
		print ">>>>>>>>h1"
		GPSLogs["xy_list"]=GPSLogs.LongLat.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["C"],map_mappings[map_mappings_name_center]))
		GPSLogs["xy_listR"]=GPSLogs.LongLat.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["R"],map_mappings[map_mappings_name_right]))
		GPSLogs["xy_listL"]=GPSLogs.LongLat.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["L"],map_mappings[map_mappings_name_left]))
		GPSLogs["xy_listZoomOut1"]=GPSLogs.LongLat.apply(lambda x :strLatLong_to_list_of_tuple(x,maps["ZoomOut1"],map_mappings[map_mappings_name_ZoomOut1]))
		print ">>>>>>>>h2"
		GPSLogs.to_csv(file_name)
	else:
		GPSLogs=GPSLogs[GPSLogs.DevId.str.contains("CLB_DIAL_ACOA")]
		pass

	print "here2",len(GPSLogs)
	start_time=GPSLogs.iloc[0].LogDate
	end_time=GPSLogs.iloc[-1].LogDate
	equipment_images={}
	unique_devices=GPSLogs.DevId.unique().tolist()
	#unique_devices=[i for i in unique_devices if ("SG" in i or "ASAT" in i or "RE" in i or "SKY" in i)]#("SG" in i or "ASAT" in i)
	for Device in unique_devices:
		if 1:# "SG" in Device or "ASAT" in Device or "RE" in Device or "SKY" in Device:
			print "{}.png".format(Device.split("_")[-2][1:].lower())
			try:
				equipment_images[Device]=pygame.image.load("images/icons/{}.png".format(Device.split("_")[-2][1:].lower()))
			except:
				try:
					equipment_images[Device]=pygame.image.load("images/icons/{}.jpg".format(Device.split("_")[-2][1:].lower()))
				except:
					equipment_images[Device]=pygame.image.load("images/icons/b1.ico")
					print Device
load_data(current_samplle_number)

seconds=0
current_curser_position=(0,0)
print "here3"
#exit()
space_bar_pressed=False
A_KEY_pressed=False
S_KEY_pressed=False
D_KEY_pressed=False
DOWN_ARROW_KEY_pressed=False
DOWN_ARROW_KEY_pressed_time=0
UP_ARROW_KEY_pressed_time=0
UP_ARROW_KEY_pressed=False
current_time=start_time
selected_pos=0,0
current_selected_device=None
equipment_last_location_dict={}
current_map_num=1
while not crashed:
	#print "gameLoop"
	last_current_time=current_time
	if not space_bar_pressed:

		current_time=current_time + pd.Timedelta(seconds=1)
		#seconds+=1
	if current_time > end_time:
		message_display("changing data sample {}".format((current_samplle_number+1)%max_sample_number),[w/2,h/2],gameDisplay)
		pygame.display.update()
		current_samplle_number+=1
		load_data(current_samplle_number%max_sample_number)
		current_time=start_time
	elif current_time < start_time:
		message_display("changing data sample {}".format((current_samplle_number-1)%max_sample_number),[w/2,h/2],gameDisplay)
		pygame.display.update()		
		current_samplle_number-=1
		load_data(current_samplle_number%max_sample_number)
		current_time=end_time
	events=pygame.event.get()
	for event in events:
		if event.type==pygame.MOUSEBUTTONDOWN:
			if event.button==3:
				current_curser_position=event.pos
			if event.button==1:
				selected_pos=event.pos				
		if event.type==pygame.QUIT:
			crashed=True
		if event.type==pygame.KEYDOWN :
			if event.key==RIGHT_ARROW_KEY:
				#print ">>>>>>>>>>>>>>>>>>>"
				if current_map_num<3:
					current_map_num=(current_map_num+1)			
			if event.key==LEFT_ARROW_KEY:
				if current_map_num>0:
					current_map_num=(current_map_num-1)							
			if event.key==SPACE_BAR_KEY:
				space_bar_pressed=not space_bar_pressed
			if event.key==A_KEY:
				A_KEY_pressed=True
			if event.key==S_KEY:
				S_KEY_pressed=True
			if event.key==D_KEY:
				D_KEY_pressed=True
			if event.key==UP_ARROW_KEY:
				UP_ARROW_KEY_pressed=True
				UP_ARROW_KEY_pressed_time=time.time()
			if event.key==DOWN_ARROW_KEY:
				DOWN_ARROW_KEY_pressed=True
				DOWN_ARROW_KEY_pressed_time=time.time()
		if event.type==pygame.KEYUP:
			if event.key==A_KEY:
				A_KEY_pressed=False
			if event.key==S_KEY:
				S_KEY_pressed=False
			if event.key==D_KEY:
				D_KEY_pressed=False
			if event.key==UP_ARROW_KEY:
				if A_KEY_pressed:
					current_time=current_time+pd.Timedelta(hours=1)
				if S_KEY_pressed:
					current_time=current_time+pd.Timedelta(minutes=1)
				if D_KEY_pressed:
					current_time=current_time+pd.Timedelta(seconds=1)				
				UP_ARROW_KEY_pressed=False
			if event.key==DOWN_ARROW_KEY:
				if A_KEY_pressed:
					current_time=current_time-pd.Timedelta(hours=1)
				if S_KEY_pressed:
					current_time=current_time-pd.Timedelta(minutes=1)
				if D_KEY_pressed:
					current_time=current_time-pd.Timedelta(seconds=1)				
				DOWN_ARROW_KEY_pressed=False															

		#	pass
		#print(event)
	if DOWN_ARROW_KEY_pressed:
		if ( ( time.time() - DOWN_ARROW_KEY_pressed_time )> 0.5):
			equipment_last_location_dict={}
			if A_KEY_pressed:
				current_time=current_time-pd.Timedelta(hours=1)
			if S_KEY_pressed:
				current_time=current_time-pd.Timedelta(minutes=1)
			if D_KEY_pressed:
				current_time=current_time-pd.Timedelta(seconds=1)		


	if UP_ARROW_KEY_pressed:
		if ((time.time() - UP_ARROW_KEY_pressed_time ) > 0.5):
			if A_KEY_pressed:
				current_time=current_time+pd.Timedelta(hours=1)
			elif S_KEY_pressed:
				current_time=current_time+pd.Timedelta(minutes=1)
			elif D_KEY_pressed:
				current_time=current_time+pd.Timedelta(seconds=1)

	gameDisplay.fill(white)
	#gameDisplayR.fill(white)
	#gameDisplayL.fill(white)
	if current_map_num==0:
		current_map=maps["L"]
	elif current_map_num==1:
		current_map=maps["C"]
	elif current_map_num==2:
		current_map=maps["R"]		
	elif current_map_num==3:
		current_map=maps["ZoomOut1"]		

	#if current_map==maps["C"] or 1:
	
	gameDisplay.blit(current_map.mapImg,(0,0))
	for keyLocation in current_map.keyLocations:
		point_num=0
		#print current_map.keyLocations
		for point in keyLocation:
			if "54R" ==name_list[current_map.keyLocations.index(keyLocation)] or 1:
				if point_num==1:
					pass
					pygame.draw.circle(current_map.mapImg, (0,0,0),point, int(50*(current_map.mapRatio/1000)),2)
					message_display("{}".format(name_list[current_map.keyLocations.index(keyLocation)]),point,gameDisplay )
				else:
					pygame.draw.circle(current_map.mapImg, (0,0,0),point, 2)
				point_num+=1
	for Location in current_map.Locations:
		xy,name=Location
		name=name.split("_")[1] if "PICKUP" in name else name
		message_display("{}".format(name),xy,gameDisplay )
		pygame.draw.circle(current_map.mapImg, (255,0,0),xy, 2)

	gameDisplay.blit(addCarImg,(10,10))
	tempLogs=GPSLogs[GPSLogs.LogDate==current_time]
	tempLogs.drop_duplicates(subset='DevId',inplace=True)	
	if current_map==maps["C"]:
		xy_list=tempLogs.xy_list.tolist()
		xy_list=[eval(str(i)) for i in xy_list]
	if current_map==maps["R"]:
		xy_list=tempLogs.xy_listR.tolist()
		xy_list=[eval(str(i)) for i in xy_list]
	if current_map==maps["L"]:
		xy_list=tempLogs.xy_listL.tolist()
		xy_list=[eval(str(i)) for i in xy_list]				
	if current_map==maps["ZoomOut1"]:
		xy_list=tempLogs.xy_listZoomOut1.tolist()
		xy_list=[eval(str(i)) for i in xy_list]						
	device_list=tempLogs.DevId.tolist()
	anyDeviceSelected=False
	for xy in range(len(xy_list)):
		
		if 1: #"SG" in device_list[xy] or "ASAT" in device_list[xy] or "RE" in device_list[xy]  or "SKY" in device_list[xy]:
			#print (device_list[xy],xy_list[xy])
			current_device=device_list[xy]
			rotated_img=equipment_images[current_device]
			try:
				track_angle=tempLogs[tempLogs.DevId==unique_devices[xy]]["TrackAngle"].values[0]
			except:
				track_angle=None
			if current_device in equipment_last_location_dict.keys() :
				pass

				# if "PBT" in current_device:
				# 	print abs(diffBearing), tempLogs[tempLogs.DevId==current_device]["AvgSpeed"].values[0] 

				if  tempLogs[tempLogs.DevId==current_device]["AvgSpeed"].values[0] > 1 and last_current_time!=current_time:#abs(diffBearing) > 15 and : 
					latlon=tempLogs[tempLogs.DevId==current_device]["LongLat"].values[0]
					current_bearing=BearingBtwCoordinates(equipment_last_location_dict[current_device]["last_LongLat"].split(","),latlon.split(","))
					#print current_device,current_bearing
					diffBearing=current_bearing - equipment_last_location_dict[current_device]["last_bearing"]
					lat2,lon2=map(float,latlon.split(","))
					lat1,lon1=map(float,equipment_last_location_dict[current_device]["last_LongLat"].split(","))
					slope_angle=(atan2(lat2-lat1,lon2-lon1)*180)/pi
					slope_angle=slope_angle if slope_angle > 0 else (180 + (180 + slope_angle)) 


					# if space_bar_pressed :
					# 	if "PBT" in current_device:
					# 		print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.."
					if track_angle is not None and 0:
						rotated_img=pygame.transform.rotate(rotated_img, track_angle )
					else:	
						rotated_img=pygame.transform.rotate(rotated_img, slope_angle )
					equipment_last_location_dict[current_device]["last_bearing"]=current_bearing
					equipment_last_location_dict[current_device]["last_LogDate"]=tempLogs[tempLogs.DevId==current_device]["LogDate"].values[0]
					equipment_last_location_dict[current_device]["last_LongLat"]=latlon
					equipment_last_location_dict[current_device]["last_slope_angle"]=slope_angle
				else:
					if track_angle is not None and 0:
						rotated_img=pygame.transform.rotate(rotated_img,track_angle)
					else:
						rotated_img=pygame.transform.rotate(rotated_img, equipment_last_location_dict[current_device]["last_slope_angle"])

				if "PBT" in current_device:
					message_display("slope=%20s"%(equipment_last_location_dict[current_device]["last_slope_angle"]),[w-500,120],gameDisplay )

			else:
				equipment_last_location_dict[current_device]={}
				equipment_last_location_dict[current_device]["last_bearing"]=0
				equipment_last_location_dict[current_device]["last_LogDate"]=tempLogs[tempLogs.DevId==current_device]["LogDate"].values[0]
				equipment_last_location_dict[current_device]["last_LongLat"]=tempLogs[tempLogs.DevId==current_device]["LongLat"].values[0]
				equipment_last_location_dict[current_device]["last_slope_angle"]=0
				#rotated_img=pygame.transform.rotate(equipment_images[current_device],0)
				pass
			gameDisplay.blit(rotated_img,(xy_list[xy][0],xy_list[xy][1]))
			pygame.draw.circle(gameDisplay, (255,0,0), (xy_list[xy][0],xy_list[xy][1]), 2)
			if distanceBetweenPoints(selected_pos,(xy_list[xy][0],xy_list[xy][1])) < 10:
				current_selected_device=current_device
				anyDeviceSelected=True
	if not anyDeviceSelected:
		current_selected_device=None
			#else:
			#	current_selected_device=None

	for xy in range(len(unique_devices)):
		#try:
		if len(tempLogs[tempLogs.DevId==unique_devices[xy]]["LongLat"].values) > 0:
			latlon=tempLogs[tempLogs.DevId==unique_devices[xy]]["LongLat"].values[0]
			refloc,refDistance=locationToRefLocation(latlon.split(",")[0],latlon.split(",")[1],unique_devices[xy])
			#print unique_devices[xy],latlon
			try:
				track_angle=tempLogs[tempLogs.DevId==unique_devices[xy]]["TrackAngle"].values[0]
			except:
				track_angle=None
			if refloc in Bay_Locations.keys():
				bay=refloc
				equipment_bearing=BearingBtwCoordinates(Bay_Locations[refloc]["D"],latlon.split(","))
				angle_btw_DA_and_Dequipment=(equipment_bearing - Bay_Locations[refloc]["BearingDA"]) if (equipment_bearing - Bay_Locations[refloc]["BearingDA"]) > 0 else (Bay_Locations[refloc]["BearingDA"] - equipment_bearing)
				angle_btw_DA_and_Dequipment= angle_btw_DA_and_Dequipment if angle_btw_DA_and_Dequipment <= 180 else (360 - angle_btw_DA_and_Dequipment ) 
				if Bay_Locations[bay]["BearingDA"] >= -180:
					if (equipment_bearing < Bay_Locations[bay]["BearingDA"] and equipment_bearing > (Bay_Locations[bay]["BearingDA"]-180) ):
						#true == left
						angle_btw_DA_and_Dequipment=angle_btw_DA_and_Dequipment
					else:
						angle_btw_DA_and_Dequipment=-angle_btw_DA_and_Dequipment
				else:
					if (equipment_bearing > Bay_Locations[bay]["BearingDA"] and equipment_bearing < (Bay_Locations[bay]["BearingDA"]+180) ):
						angle_btw_DA_and_Dequipment=-angle_btw_DA_and_Dequipment
					else:
						angle_btw_DA_and_Dequipment=angle_btw_DA_and_Dequipment
				distance_D_equipment=distanceBtwCoordinates(latlon.split(","),Bay_Locations[refloc]["D"])	
				parallel_distance= distance_D_equipment*cos( (angle_btw_DA_and_Dequipment*pi)/180.0 )
				perpendicular_distance= distance_D_equipment*sin( (angle_btw_DA_and_Dequipment*pi)/180.0 )
				if "COA_0009" in unique_devices[xy] or "COA_0005" in unique_devices[xy]:
					#print "here13",unique_devices[xy]
					pass
				specific_loc=BayToSpecificLocation(refloc,latlon.split(","),unique_devices[xy])
			else:
				if "COA_0009" in unique_devices[xy] or "COA_0005" in unique_devices[xy]:
					#print "here13",unique_devices[xy]
					pass
				distance_D_equipment="NA"
				equipment_bearing="NA"
				angle_btw_DA_and_Dequipment="NA"
				parallel_distance="NA"
				perpendicular_distance="NA"
				specific_loc="NA"
				track_angle=None
		else:			
			if "COA_0009" in unique_devices[xy] or "COA_0005" in unique_devices[xy]:
				#print "here13",unique_devices[xy]
				pass
			distance_D_equipment="NAA"
			refloc="NAAA"
			refDistance=9999
			equipment_bearing="NAA"
			angle_btw_DA_and_Dequipment="NAA"
			parallel_distance="NAA"
			perpendicular_distance="NAA"
			specific_loc="NAA"	
			track_angle=None	
		if current_selected_device in unique_devices and current_selected_device==unique_devices[xy]:				
			s="{}:s={},refl={},refD={},b={},ang={},ll_d={},_l_d={},specl={},ta={}".format(unique_devices[xy],tempLogs[tempLogs.DevId==unique_devices[xy]].AvgSpeed.values,refloc,refDistance,equipment_bearing,angle_btw_DA_and_Dequipment,(parallel_distance - (Bay_Locations[refloc]["DA"])) if (parallel_distance> 0 and refloc in Bay_Locations.keys())  else parallel_distance ,perpendicular_distance,specific_loc,track_angle)
			message_display(s,[w-(w/3),600],gameDisplay)
			#print "========",s
		elif current_selected_device is None:
			s= "{}:s={},refl={},refD={},b={},ang={},ll_d={},_l_d={},specl={},ta={}".format(unique_devices[xy],tempLogs[tempLogs.DevId==unique_devices[xy]].AvgSpeed.values,refloc,refDistance,equipment_bearing,angle_btw_DA_and_Dequipment,(parallel_distance - (Bay_Locations[refloc]["DA"])) if (parallel_distance> 0 and refloc in Bay_Locations.keys())  else parallel_distance ,perpendicular_distance,specific_loc,track_angle)
			message_display(s,[w-(w/3),600+xy*20],gameDisplay)
			#print ">>>>>>>>>>>>>>>",s

	current_map.updateMap()
	temp=pixles_to_latlong(current_curser_position,current_map)
	message_display("pixels=({},{}) latLong=({},{}),grid={}".format(current_curser_position[0],current_curser_position[1],temp[0],temp[1],gpsToGridNo(temp[0],temp[1])),[w-500,50],gameDisplay)
	message_display(datetime.strftime(datetime.now(),"%y-%m-%d %H:%M:%S"),[w-500,100],gameDisplay )
	message_display(str(current_time),[w-500,150],gameDisplay)
	bay_cnt=1
	for bay in Bay_Locations.keys():
		if bay[0]=="6":
			#message_display("bay= {},DC={},DB={},DA={},BearingDA={}".format(bay,Bay_Locations[bay]["DC"],Bay_Locations[bay]["DB"],Bay_Locations[bay]["DA"],Bay_Locations[bay]["BearingDA"]),[w-500,150+20*bay_cnt],gameDisplay)
			bay_cnt+=1
	# for equipment in current_map.onMapEquipmentList:
	# 	equipment.update_on_display()
	# for b in current_map.beaconList:
	# 	#print "beacn position",[b.blitPosition[0],b.blitPosition[1]]
	# 	gameDisplay.blit(b.img,[b.blitPosition[0],b.blitPosition[1]])
	# 	pygame.draw.circle(gameDisplay, (255,0,0), (int(b.position[0]),int(b.position[1])), 2)	
	pygame.display.update()
	clock.tick(120)
pygame.quit()
quit()









#def process_events(self,event):
	
