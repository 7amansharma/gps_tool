import os
import sys
import struct
import MySQLdb
import datetime
import time
import json
import collections
from django.http import HttpResponse
import pandas as pd 
from sqlalchemy import create_engine

import logging

import pika
from pika.exceptions import ConnectionClosed, ChannelClosed
from psutil import virtual_memory
#from pyrabbit import http


class RabbitMQMgmt:
	def __init__(self, host='localhost', port=5672, virtual_host='/', username='guest', password='guest',
				 base_api_url='localhost:15672'):
		"""
		Initializes connection and channel; sets them to None if error occurs
		:param host: RabbitMQ server hostname/ip (str) (default: 'localhost')
		:param port: RabbitMQ server port number (int) (default: 5672)
		:param virtual_host: RabbitMQ virtual host name (str) (default: '/')
		:param username: RabbitMQ server username (str) (default: 'guest')
		:param password: RabbitMQ server password (str) (default: 'guest')
		:param base_api_url: RabbitMQ management api base URL (str) (default: 'localhost:15672')
		"""
		if not os.path.isdir('logs'):
			os.makedirs('logs')

		logging.basicConfig(filename='./logs/rabbitmq_mgmt_logs.txt', level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S',
							format='(%(asctime)s.%(msecs)03d %(levelname)s: %(message)s')
		logging.getLogger("pika").setLevel(logging.WARNING)

		self.base_api_url = base_api_url
		self.username = username
		self.password = password
		creds = pika.PlainCredentials(username=username, password=password, erase_on_connect=True)
		try:
			self.conn = pika.BlockingConnection(pika.ConnectionParameters(host=host, port=port,
																		  virtual_host=virtual_host, credentials=creds))
			self.channel = self.conn.channel()
		except ConnectionClosed or ChannelClosed:
			self.conn, self.channel = None, None

	# noinspection PyBroadException
	def __del__(self):
		"""
		Destructor
		:return: None
		"""
		try:
			self.channel.close()
		except Exception:
			pass
		try:
			self.conn.close()
		except Exception:
			pass

	def create_queue(self, name='', passive=False, durable=False, exclusive=False, auto_del=False, args=None):
		"""
		Creates a queue
		:param name: name of queue to be created (str) (default: '')
		:param passive: only check to see if the queue exists (bool) (default: False)
		:param durable: survive reboots of the broker (bool) (default: False)
		:param exclusive: only allow access by the current connection (bool) (default: False)
		:param auto_del: delete after consumer cancels or disconnects (bool) (default: False)
		:param args: custom key/value arguments for the queue (dict) (default: None)
		:return: queue method object if success, None if failure
		"""
		try:
			if not self.channel:
				raise ConnectionClosed
			q = self.channel.queue_declare(queue=name, passive=passive, durable=durable, exclusive=exclusive,
										   auto_delete=auto_del, arguments=args)
			return q
		except ValueError as e:
			logging.error('ValueError in create_queue(): {}'.format(e))
			return None
		except ConnectionClosed as e:
			logging.error('ConnectionClosed in create_queue(): {}'.format(e))
			return None
		except ChannelClosed as e:
			logging.error('ChannelClosed in create_queue(): {}'.format(e))
			return None

	def del_queue(self, name, if_unused=False, if_empty=False):
		"""
		Deletes a particular queue
		:param name: name of queue to be deleted (str)
		:param if_unused: only delete if it's unused (bool) (default: False)
		:param if_empty: only delete if the queue is empty (bool) (default: False)
		:return: True/False - success/failure (bool)
		"""
		try:
			if not self.channel:
				raise ConnectionClosed
			self.channel.queue_delete(queue=name, if_unused=if_unused, if_empty=if_empty)
			return True
		except ValueError as e:
			logging.error('ValueError in del_queue(): {}'.format(e))
			return False
		except ConnectionClosed as e:
			logging.error('ConnectionClosed in del_queue(): {}'.format(e))
			return False

	def purge_queue(self, name):
		"""
		Purges a particular queue (delete all messages from the queue w/o deleting the queue)
		:param name: name of queue to be purged (str)
		:return: True/False - success/failure (bool)
		"""
		try:
			if not self.channel:
				raise ConnectionClosed
			self.channel.queue_purge(queue=name)
			return True
		except ValueError as e:
			logging.error('ValueError in purge_queue(): {}'.format(e))
			return False
		except ConnectionClosed as e:
			logging.error('ConnectionClosed in purge_queue(): {}'.format(e))
			return False

	def get_num_messages(self, name):
		"""
		Get number of messages in a queue
		:param name: name of queue of which number of messages are to be known (str)
		:return: number of messages (-1 if failure)
		"""
		q = self.create_queue(name=name, passive=True)
		if q:
			return q.method.message_count
		else:
			return -1

	# TODO node
	# def get_memory_usage(self, name=None, node='rabbit@zestiot-15'):
	# 	"""
	# 	Get RAM usage of a particular queue or all of RabbitMQ
	# 	:param name: name of queue whose RAM consumption is to be known. If it is None, then total RAM consumption by
	# 				 RabbitMQ is returned (str) (default: None)
	# 	:param node: node name (str) (optional)
	# 	:return: Percentage RAM consumption by queue/RabbitMQ out of total RAM of the system (float) (-1 is returned if
	# 	error)
	# 	"""
	# 	total_ram = float(virtual_memory().total)
	# 	try:
	# 		http_client = http.HTTPClient(server=self.base_api_url, uname=self.username, passwd=self.password)
	# 		if name:
	# 			api_path = 'queues/%2f/{}'.format(name)
	# 			res = http_client.do_call(api_path, 'GET')
	# 			ram = res['memory'] + res['message_bytes_ram']
	# 		else:
	# 			api_path = 'nodes/{}/memory'.format(node)
	# 			res = http_client.do_call(api_path, 'GET')
	# 			ram = float(res['memory']['total'])
	# 		return ram / total_ram * 100.0
	# 	except http.HTTPError or http.NetworkError as e:
	# 		logging.error('http.HTTPError or http.NetworkError in get_memory_usage(): {}'.format(e))
	# 		return -1

	def create_exchange(self, name, exchange_type='direct', durable=False, auto_del=False, internal=False, args=None):
		"""
		Creates an exchange
		:param name: exchange name (str)
		:param exchange_type: exchange type (str) (default: 'direct')
		:param durable: survive a reboot of RabbitMQ (bool) (default: False)
		:param auto_del: remove when no more queues are bound to it (bool) (default: False)
		:param internal: can only be published to by other exchanges (bool) (default: False)
		:param args: custom key/value pair arguments for the exchange (dict) (default: None)
		:return: True/False - success/failure (bool)
		"""
		try:
			if not self.channel:
				raise ConnectionClosed
			self.channel.exchange_declare(exchange=name, exchange_type=exchange_type, durable=durable,
										  auto_delete=auto_del, internal=internal, arguments=args)
			return True
		except ValueError as e:
			logging.error('ValueError in create_exchange(): {}'.format(e))
			return False
		except ConnectionClosed as e:
			logging.error('ConnectionClosed in create_exchange(): {}'.format(e))
			return False

	def del_exchange(self, name, if_unused=False):
		"""
		Deletes an exchange
		:param name: exchange name (str)
		:param if_unused: only delete if the exchange is unused (bool) (default: False)
		:return: True/False - success/failure (bool)
		"""
		try:
			if not self.channel:
				raise ConnectionClosed
			self.channel.exchange_delete(exchange=name, if_unused=if_unused)
			return True
		except ValueError as e:
			logging.error('ValueError in del_exchange(): {}'.format(e))
			return False
		except ConnectionClosed as e:
			logging.error('ConnectionClosed in del_exchange(): {}'.format(e))
			return False

	def publish(self, exchange, routing_key, body, properties=None, mandatory=False, immediate=False):
		"""
		Publish to the channel with the given exchange, routing key and body
		:param exchange: the exchange to publish to (str)
		:param routing_key: the routing key to bind on (str)
		:param body: the message body (str)
		:param properties: Basic.properties (pika.spec.BasicProperties)
		:param mandatory: the mandatory flag (bool) (default: False)
		:param immediate: the immediate flag (bool) (default: False)
		:return: None
		"""
		try:
			if not self.channel:
				raise ConnectionClosed
			self.channel.basic_publish(exchange=exchange, routing_key=routing_key, body=body, properties=properties,
									   mandatory=mandatory, immediate=immediate)
			return True
		except ConnectionClosed as e:
			logging.error('ConnectionClosed in publish(): {}'.format(e))
			return False

	def bind_exchange(self, dest, src, routing_key, args=None):
		"""
		Bind an exchange to another
		:param dest: destination exchange to bind (str)
		:param src: source exchange to bind to (str)
		:param routing_key: routing key to bind on (str)
		:param args: custom key/value pair arguments for the binding (dict) (default: None)
		:return: True/False - success/failure (bool)
		"""
		try:
			if not self.channel:
				raise ConnectionClosed
			self.channel.exchange_bind(destination=dest, source=src, routing_key=routing_key, arguments=args)
			return True
		except ValueError as e:
			logging.error('ValueError in del_queue(): {}'.format(e))
			return False
		except ConnectionClosed as e:
			logging.error('ConnectionClosed in del_queue(): {}'.format(e))
			return False

	def unbind_exchange(self, dest, src, routing_key, args=None):
		"""
		Unbind an exchange from another exchange.
		:param dest: destination exchange to unbind (str)
		:param src: source exchange to unbind from (str)
		:param routing_key: routing key to unbind (str)
		:param args: custom key/value pair arguments for the binding (dict) (default: None)
		:return: True/False - success/failure (bool)
		"""
		try:
			if not self.channel:
				raise ConnectionClosed
			self.channel.exchange_bind(destination=dest, source=src, routing_key=routing_key, arguments=args)
			return True
		except ValueError as e:
			logging.error('ValueError in del_queue(): {}'.format(e))
			return False
		except ConnectionClosed as e:
			logging.error('ConnectionClosed in del_queue(): {}'.format(e))
			return False

	def bind_queue(self, queue, exchange, routing_key=None, args=None):
		"""
		bind the queue to the specified exchange
		:param queue: the queue to bind to the exchange (str)
		:param exchange: the source exchange to bind to (str)
		:param routing_key: the routing key to bind on (str)
		:param args: custom key/value pair arguments for the binding (dict) (default: None)
		:return: True/False - success/failure (bool)
		"""
		try:
			if not self.channel:
				raise ConnectionClosed
			self.channel.queue_bind(queue=queue, exchange=exchange, routing_key=routing_key, arguments=args)
			return True
		except ValueError as e:
			logging.error('ValueError in bind_queue(): {}'.format(e))
			return False
		except ConnectionClosed as e:
			logging.error('ConnectionClosed in bind_queue(): {}'.format(e))
			return False

	def unbind_queue(self, queue, exchange=None, routing_key=None, args=None):
		"""
		unbind a queue from an exchange
		:param queue: the queue to unbind from the exchange (str)
		:param exchange: the source exchange to bind from (str)  (default: None)
		:param routing_key: the routing key to unbind (str) (default: None)
		:param args: custom key/value pair arguments for the binding (dict) (default: None)
		:return: True/False - success/failure (bool)
		"""
		try:
			if not self.channel:
				raise ConnectionClosed
			self.channel.queue_unbind(queue=queue, exchange=exchange, routing_key=routing_key, arguments=args)
			return True
		except ValueError as e:
			logging.error('ValueError in unbind_queue(): {}'.format(e))
			return False
		except ConnectionClosed as e:
			logging.error('ConnectionClosed in unbind_queue(): {}'.format(e))
			return False




class Utility:
	ConfigFilePath="Config.json"
	mode = 0
	debug = 0
	def __init__(self, Idebug = 0, logFile = None, db = None,mode=1,tag="",enable_db=True,enable_raabbitmq=False,ConfigFilePath=None):
		self.enable_raabbitmq=enable_raabbitmq
		self.enable_db=enable_db
		self.debug = Idebug
		#if Idebug==0:
		self.mode=mode
		self.tag=tag
		self.sql_engine=None
		self.config_json=None
		if ConfigFilePath is not None:
			self.ConfigFilePath=ConfigFilePath
		self.init_config(logFile, db)		
		
	def loginfo(self, msg):
		#print self.mode,self.debug,"loginfo"
		msg=str(self.tag)+" :"+msg
		if self.mode == 0 and self.debug == 1:
			print msg
		elif self.mode == 1 and self.debug == 1:
			CurrentDT = datetime.datetime.now()
			CurrentDTStr = CurrentDT.strftime("%d-%m-%y %H:%M:%S")
			msg = CurrentDTStr + ":" +  msg
			fo = open(self.logFile,'a+')
			print >>fo,msg + ""
			fo.flush()
			os.fsync(fo.fileno())
			fo.close()


	def logWarning(self, msg):
		#print self.mode,self.debug,"loginfo"
		if self.mode == 0 and self.debug == 1:
			print msg
		elif (self.mode == 1 or self.mode==2) and self.debug == 1:
			CurrentDT = datetime.datetime.now()
			CurrentDTStr = CurrentDT.strftime("%d-%m-%y %H:%M:%S")
			msg = CurrentDTStr + ":" +  msg
			fo = open(self.logFile,'a+')
			print >>fo,msg + ""
			fo.flush()
			os.fsync(fo.fileno())
			fo.close()	

	def logError(self, msg):
		#print self.mode,self.debug,"loginfo"
		try:
			print "this error 1"
			print msg
			if self.mode == 0 and self.debug == 1:
				print msg
			elif (self.mode == 1 or self.mode==2 or self.mode==3) and self.debug == 1:
				CurrentDT = datetime.datetime.now()
				CurrentDTStr = CurrentDT.strftime("%d-%m-%y %H:%M:%S")
				msg = CurrentDTStr + ":" +  msg
				fo = open(self.logFile,'a+')
				print >>fo,msg + ""
				fo.flush()
				os.fsync(fo.fileno())
				fo.close()	
		except:
			pass


# def init_config(self, logFileName):
	def init_config(self, logFile, db):
		#print os.path.abspath(__file__)
		print "here"
		try:
			print "here1"
			self.config_json = json.loads(open(self.ConfigFilePath).read())
			print "here01"
			self.unprocessed = self.config_json['_unprocessed']
			self.processed = self.config_json['_processed']
			self.sleepTime = self.config_json['_sleep_time']
			if logFile:
					self.logFile = str(os.path.join(self.config_json["_logs"], logFile))
					#self.mode = 1
				#else:
					#self.mode = 0				

		except Exception as e:
			#print "error reading config file",str(e)
				self.logError("error reading config file"+str(e))
		if self.enable_db:
			try:
				print "here2"
				# Initialize database
				self.db = MySQLdb.connect( 
					host=self.config_json["_mysql"]["host"],
					user=self.config_json["_mysql"]["user"],
					passwd=self.config_json["_mysql"]["passwd"],
					db=self.config_json["_mysql"]["db"])
				self.db.autocommit(True)
				print "here20"
			except Exception as e:
				#print "error in connecting to mysql server :",str(e)
				self.logError("error in connecting to mysql server :"+str(e))
			print "here3"

	def query_database(self, sql):
		cursor = self.db.cursor()
		self.loginfo("Read Query: " + sql)
		try:
			t=time.time()
			cursor.execute(sql)
			rows = cursor.fetchall()
			no_of_rows = cursor.rowcount
			# return (rows,no_of_rows)
			tdiff=time.time() - t
			self.loginfo( "Success, NoOfRows: " + str(no_of_rows) +" timeTaken= "+str(tdiff)+"secs")
			return (rows, no_of_rows)
		except Exception as e:
			#print "Error in executing sql " + str(e) 
			self.logError( "Error in executing sql " + str(e))
			if "Lost connection to MySQL server during query" in str(e) or "MySQL server has gone away" in str(e):
				try:
					try:
						self.close_connection()
					except:
						pass
					self.init_config(self.logFile,None)
					cursor = self.db.cursor()
					cursor.execute(sql)
					rows = cursor.fetchall()
					no_of_rows = cursor.rowcount
					# return (rows,no_of_rows)
					self.loginfo( "Success, NoOfRows: " + str(no_of_rows))
					return (rows, no_of_rows)
				except Exception as e:
						self.logError( "Error in retrying connecting to mysql server " + str(e))
			return None,None

		def query_database_data(self, sql):
				cursor = self.db.cursor()
				self.loginfo("Read Query :"+sql)
				try:
					cursor.execute(sql)
					rows = cursor.fetchall()
					return rows
				except Exception as e:
					self.logerror("Error in executing sql",str(e))
					return None  

	def query_database_df(self, sql):
		cursor = self.db.cursor()
		self.loginfo("Read Query: " + sql)
		try:
			t=time.time()
			df = pd.read_sql(sql, con=self.db)
			tdiff=time.time() -t
			self.loginfo("time taken = "+str(tdiff))
			return df
		except Exception as e:
			#print "Error in executing sql ", str(e)
			self.logError( "Error in executing sql " + str(e))
			if "Lost connection to MySQL server during query" in str(e) or "MySQL server has gone away" in str(e):
				try:
					try:
						self.close_connection()
					except:
						pass
					self.init_config(self.logFile,None)
					cursor = self.db.cursor()
					df = pd.read_sql(sql, con=self.db)
					return df
				except Exception as e:
						self.logError( "Error in retrying connecting to mysql server " + str(e))			
			return None


	def update_database(self, sql):
		cursor = self.db.cursor()
		self.loginfo("Update Query: " + sql)
		try:
			t=time.time()
			cursor.execute(sql)
			rows = cursor.fetchall()
			self.db.commit()
			#print "Update Success"
			tdiff=time.time() - t
			self.loginfo( "Update Success , timeTaken = "+str(tdiff))
			return True
		except Exception as e:
			#print "Error in executing sql" + str(e)
			print "here1"
			self.logError( "Error in executing sql >>" + str(e))
			#self.db.rollback()
			print "here2"
			if "Lost connection to MySQL server during query" in str(e) or "MySQL server has gone away" in str(e):
				try:
					try:
						self.close_connection()
					except:
						pass
					self.init_config(self.logFile,None)
					cursor = self.db.cursor()
					cursor.execute(sql)
					rows = cursor.fetchall()
					self.db.commit()
					#print "Update Success"
					self.loginfo( "Update Success")
					return True
				except Exception as e:
						self.logError( "Error in retrying connecting to mysql server " + str(e))					
			return False

	def update_database2(self, sql):
		cursor = self.db.cursor()
		self.loginfo("Update Query: " + sql)
		try:
			t=time.time()
			rows_affected= cursor.execute(sql)
			rows = cursor.fetchall()
			self.db.commit()
			#print "Update Success"
			tdiff=time.time() - t
			self.loginfo( "Update Success , timeTaken = "+str(tdiff))
			return cursor
		except Exception as e:
			#print "Error in executing sql" + str(e)
			print "here1"
			self.logError( "Error in executing sql >>" + str(e))
			#self.db.rollback()
			print "here2"
			if "Lost connection to MySQL server during query" in str(e) or "MySQL server has gone away" in str(e):
				try:
					try:
						self.close_connection()
					except:
						pass
					self.init_config(self.logFile,None)
					cursor = self.db.cursor()
					cursor.execute(sql)
					rows = cursor.fetchall()
					self.db.commit()
					#print "Update Success"
					self.loginfo( "Update Success")
					return True
				except Exception as e:
						self.logError( "Error in retrying connecting to mysql server " + str(e))					
			return False			

	def errorlog(self, file, msg,flag,cnt,er_cnt,ptime,ttime,ram,gp_cnt,ver):
		try:
			if (self.mode == 0 or self.mode == 1 or self.mode==2 or self.mode==3) and self.debug ==1:
				FMT= "%Y-%m-%d %H:%M:%S"
				device_id_list,no_device=self.query_database("select DevId from EquipmentMaster")
				fname = (file).split("/")[-1]
				for i in device_id_list:
					i=i[0]
					j=i.replace("_","")
					#self.loginfo("j =" +str(j)+"fname= "+str(fname))
					if j in fname:
						devId= i
						break
					else:
						devId = "Not registered"
				if len(flag) > 0:
					self.loginfo("Error in file: "+str(fname)+"Error : "+str(msg))
					status = 1
					fl=int(flag)
				else:
					self.loginfo("no error in file")
					status = 0
					fl=0
				print type(flag)
				CurrentDT = datetime.datetime.now()
				CurrentDTStr = CurrentDT.strftime(FMT)
				if ptime is None and cnt is None:
					sql="INSERT INTO ProcessedFiles (FileName,TimeStamp,Error_status,Error_flag,DevId) Values('{}','{}','{}','{}','{}');".format(fname,CurrentDTStr,status,fl,devId)
				elif ptime is None:
					sql="INSERT INTO ProcessedFiles (FileName,TimeStamp,Error_status,Error_flag,DevId,No_of_Rows,No_of_rows_without_data) Values('{}','{}','{}','{}','{}','{}','{}');".format(fname,CurrentDTStr,status,fl,devId,cnt,er_cnt)
				else:
					print ptime,type(ptime),ttime,type(ttime),ptime.strip(),ttime.strip()
					lag=datetime.datetime.strptime(ptime.strip(), FMT) - datetime.datetime.strptime(ttime.strip(), FMT)
					lag=lag.total_seconds()
					gps_count = gp_cnt - er_cnt
					sql="INSERT INTO ProcessedFiles (FileName,TimeStamp,Error_status,Error_flag,DevId,No_of_Rows,No_of_rows_without_data,Timestamp_device,Timestamp_platform,Lag_sec,RamUtilized,GPS_count,Version) Values('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}');".format(fname,CurrentDTStr,status,fl,devId,cnt,er_cnt,ttime,ptime,lag,ram,gps_count,ver)
				self.update_database(sql)
		except Exception as e:
			self.loginfo("Exception : "+str(e))

	def update_database_df(self, table_name,df):
		#cursor = self.db.cursor()
		self.loginfo("Update table: " + table_name)
		try:
			if self.config_json is None:
				self.config_json = json.loads(open(self.ConfigFilePath).read())
			if self.sql_engine is None: 
				self.sql_engine = create_engine("mysql+mysqldb://"+str(self.config_json["_mysql"]["user"])+":"+self.config_json["_mysql"]["passwd"]+"@"+str(self.config_json["_mysql"]["host"])+"/"+self.config_json["_mysql"]["db"],pool_recycle=3600,pool_pre_ping=True)
			t=time.time()
			df.to_sql(con=self.sql_engine,name=table_name,if_exists='append',index=False)
			tdiff=time.time() - t
			self.loginfo("TimeTaken= "+str(tdiff))
			return True
			#cursor.execute(sql)
			#rows = cursor.fetchall()
			#self.db.commit()
			#self.loginfo( "Update Success")
			#print df.to_sql(con=self.db, name=table_name, flavor='mysql')
		except Exception as e:
			#print "Error in updating table" ,str(e)
			self.logError( "Error in updating table" + str(e))
			# try:
			# 	self.db.rollback()
			# except:
			# 	pass
			#quit()
			return False

	def get_unique_uuid(self):
		sql = "Select UUID();"
		self.loginfo("Query: " + sql)
		try:
			df = pd.read_sql(sql, con=self.db)
			return df.get_value(0, 'UUID()', takeable=False)
		except Exception as e:
			self.logError( "Error in executing sql" + str(e))
			self.db.rollback()
			return None

	def check_params(self, request, param, man):
		if request.POST.get(param):
			return request.POST.get(param)
		elif man == True:
			return HttpResponse("{ "+param+" is missing.}")
		else:
			return False

	def close_connection(self):
		try :
			self.db.close()
		except Exception as e:
			self.loginfo("exception in closing connection :"+str(e))







if __name__ == "__main__":
	utility = Utility("Cleanup.txt")
	# //clear SensorLogs
	# delete_date = (datetime.datetime.now() -  datetime.timedelta(days=15)).strftime('%Y-%m-%d')
	# clean_sensor_logs_query = "Delete from SensorLogs where LogDate<='%s' and Status=1;"%(str(delete_date))
	# utility.update_database(clean_sensor_logs_query)


	# # //clear MovementLogs
	# delete_date = (datetime.datetime.now() -  datetime.timedelta(days=30)).strftime('%Y-%m-%d')
	# clean_sensor_logs_query = "Delete from MovementLogs where FLogDate<='%s' and Status=1;"%(str(delete_date))
	# utility.update_database(clean_sensor_logs_query)


	# # //clear GPSMovementLogs
	# delete_date = (datetime.datetime.now() -  datetime.timedelta(days=30)).strftime('%Y-%m-%d')
	# clean_sensor_logs_query = "Delete from GPSMovementLogs where FLogDate<='%s' and Status=1;"%(str(delete_date))
	# utility.update_database(clean_sensor_logs_query)


	# # //clear WIfiLogs
	# delete_date = (datetime.datetime.now() -  datetime.timedelta(days=30)).strftime('%Y-%m-%d')
	# clean_sensor_logs_query = "Delete from WifiLogs where LogDate<='%s' and Status=1;"%(str(delete_date))
	# utility.update_database(clean_sensor_logs_query)

	utility = Utility()
	utility.get_unique_uuid()




