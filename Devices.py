import numpy as np
from datetime import datetime
import pygame
import geopy
from geopy.distance import VincentyDistance
import os
from math import *

event_count=1
CTRL_KEY=306
LEFT_ARROW_KEY=275
RIGHT_ARROW_KEY=276
W_KEY=119
S_KEY=115
SPACE_BAR_KEY=32
A_KEY=97
M_KEY=109
SHIFT_KEY=304
B_KEY=98
D_KEY=100
DELETE_KEY=127
P_KEY=112
L_KEY=108
K_KEY=107
NUMERIC_PLUS_KEY=270
NUMERIC_MINUS_KEY=269


def text_objects(text, font):
    textSurface = font.render(text, True, (0,0,0))
    return textSurface, textSurface.get_rect()
def message_display(text,position,gameDisplay):
    largeText = pygame.font.Font('freesansbold.ttf',20)
    TextSurf, TextRect = text_objects(text, largeText)
    TextRect.center = position
    gameDisplay.blit(TextSurf, TextRect)
    #print "here"


def distanceBtwCoordinates(loc1,loc2):
    try:
        #utility.loginfo(str(loc1)+" "+str(loc2))
        loc1_lat_radians=radians(float(loc1[0]))
        #print(loc1_lat_radians)
        loc1_long_radians=radians(float(loc1[1]))
        loc2_lat_radians=radians(float(loc2[0]))
        loc2_long_radians=radians(float(loc2[1]))
        diff_lat=loc1_lat_radians - loc2_lat_radians
        diff_long=loc1_long_radians - loc2_long_radians
        intermediate_result=sin(diff_lat/2)*sin(diff_lat/2) + cos(loc1_lat_radians)*cos(loc2_lat_radians)\
        *sin(diff_long/2)*sin(diff_long/2)
        return (6371000*(2*atan2(sqrt(intermediate_result),sqrt(1-intermediate_result))))  + 0.000001
    except Exception as e:
        #utility.loginfo("Exception distanceBtwCoordinates :"+str(e)+str(traceback.format_exc())+" loc1,loc2= "+str(loc1)+" "+str(loc2))
        return 9999	
    # loc1_lat_radians=np.radians(float(loc1[0]))
    # #print(loc1_lat_radians)
    # loc1_long_radians=np.radians(float(loc1[1]))
    # loc2_lat_radians=np.radians(float(loc2[0]))
    # loc2_long_radians=np.radians(float(loc2[1]))
    # diff_lat=loc1_lat_radians - loc2_lat_radians
    # diff_long=loc1_long_radians - loc2_long_radians
    # intermediate_result=np.sin(diff_lat/2)*np.sin(diff_lat/2) + np.cos(loc1_lat_radians)*np.cos(loc2_lat_radians)*np.sin(diff_long/2)*np.sin(diff_long/2)
    # return (6371000*(2*np.arctan2(np.sqrt(intermediate_result),np.sqrt(1-intermediate_result))))  + 0.000001

def latLon_to_xy(mapObject,reference_gps_mapping_array,latlon_tuple):
	x_distance=distanceBtwCoordinates([reference_gps_mapping_array[2],reference_gps_mapping_array[3]],[reference_gps_mapping_array[2],latlon_tuple[1]])/1000.0
	y_distance=distanceBtwCoordinates([reference_gps_mapping_array[2],reference_gps_mapping_array[3]],[latlon_tuple[0],reference_gps_mapping_array[3]])/1000.0
	x_distance=x_distance*mapObject.mapRatio
	y_distance=y_distance*mapObject.mapRatio
	dx=latlon_tuple[1]-reference_gps_mapping_array[3]
	dy=latlon_tuple[0]-reference_gps_mapping_array[2]
	if dx>0:
		x=reference_gps_mapping_array[0]+x_distance
	else:
		x=reference_gps_mapping_array[0]-x_distance
	if dy>0:
		y=reference_gps_mapping_array[1]-y_distance
	else:
		y=reference_gps_mapping_array[1]+y_distance
	return [int(x),int(y)]

def pixles_to_latlong(position,mapObject):
	referenceMapping,referenceOrigin=[mapObject.point_mapping1[0],mapObject.point_mapping1[1]],geopy.Point(mapObject.point_mapping1[2],mapObject.point_mapping1[3])
	d=np.sqrt(np.power(position[0] - referenceMapping[0],2) + np.power(position[1] - referenceMapping[1],2))/(mapObject.mapRatio)
	###print "d",d,self.position[0],self.startPosition[0],(self.position[0] - self.startPosition[0])
	if position[0] == referenceMapping[0]:
		b=0
	else:
		b=90 + np.arctan2((position[1] - referenceMapping[1]),(position[0] - referenceMapping[0]))*(180/np.pi) 
	###print "b",b,90-b
	destination = VincentyDistance(kilometers=d).destination(referenceOrigin, b)
	#self.lat2, self.lon2 = 
	return destination.latitude, destination.longitude


def distanceBetweenPoints(point1,point2):
	return np.sqrt(np.power(point1[0] - point2[0],2) + np.power(point1[1] - point2[1],2))

def resample_route(route_arr,num_new_samples):
    if(num_new_samples > route_arr.shape[0]):
        ###print "here1"
        idx=np.array(range(route_arr.shape[0]))
        #print "route_arr[:,0]",route_arr.shape
        x=np.interp(np.asarray(list(range(num_new_samples))),(idx*num_new_samples)/len(idx),route_arr[:,0])
        y=np.interp(np.array(range(num_new_samples)),(idx*num_new_samples)/len(idx),route_arr[:,1])
        return np.column_stack((x,y))
    else:
        ###print "here2"
        ##print route_arr.shape[0],num_new_samples,route_arr.shape[0]/num_new_samples
        result= route_arr[::route_arr.shape[0]/num_new_samples]
        ##print len(result)
        result=result[:num_new_samples]
        result[-1]=route_arr[-1]
        return result
def smallest_reference(mapping_dict,current_point):
	if mapping_dict is not None:
		dist_list=[]
		array=mapping_dict.values()
		for i in array:
			dist_list.append(distanceBetweenPoints([i[0],i[1]],[current_point[0],current_point[1]]))
		min_dist_index=dist_list.index(min(dist_list))
		return [array[min_dist_index][0],array[min_dist_index][1]],geopy.Point(array[min_dist_index][2], array[min_dist_index][3])
	else:
		return 0

class Type1:
	def __init__(self,img_path,devId,position=[306,168],startTimeOffset=0,speed=1,bearing=90,rotate_step=15,
		speed_step=0.5,generateData=False,mapRatio=4.348808057304251*1000,
		gpsOrigin=[17.417346,78.408824],pro_dir='./processed',unpro_dir='./unprocessed',manual=True,route=[],angle_offset=0):
		self.devId=devId
		self.position=position
		self.startPosition=[position[0],position[1]]
		self.startTimeOffset=startTimeOffset
		self.speed=speed
		self.bearing=bearing
		self.img=pygame.image.load(img_path)
		self.rotate_img=False
		self.angle=90 - self.bearing -angle_offset
		self.rotate_step=rotate_step
		self.speed_step=speed_step
		self.mapRatio=mapRatio
		if self.speed>0:
			self.running=True
		else:
			self.running=False
		self.gpsOrigin=geopy.Point(gpsOrigin[0], gpsOrigin[1])
		self.referenceMapping=None
		self.referenceOrigin=None
		self.pro_dir=pro_dir
		self.unpro_dir=unpro_dir
		self.generateData=generateData
		self.one_second_event_type=-1
		self.fifteen_second_event_type=-1
		self.rect=pygame.Rect(self.position[0],self.position[1],self.img.get_width(),self.img.get_height())
		self.drag=False
		self.map=None
		self.route=route
		self.route_counter=0
		self.manual=manual
		self.ctrl_pressedDown=False
		self.shift_pressed=False
		self.making_route=False
		self.route_resampled=False
		self.modify_saved_route=False
		self.fifteen_second_done=False
		self.nearBeaconsList=[]
		self.attachedBeacons=[]
	def mouse_event(self,event):
		if event.type==pygame.MOUSEMOTION:
			if self.drag==True:
				self.position[0],self.position[1]=event.pos
				self.angle= 180 +np.arctan2((self.position[1] - self.startPosition[1]),(self.position[0] - self.startPosition[0]))*(180/np.pi)
		elif event.button==1:
			##print "here0"
			if not self.ctrl_pressedDown:
				if self.rect.collidepoint(event.pos):
					if event.type==pygame.MOUSEBUTTONDOWN:
						self.map.selected_equipment=self
						self.drag=True
				if event.type==pygame.MOUSEBUTTONUP:
						self.drag=False
			else:
				##print "here1"
				print self.devId,self.route
				if self.manual==False and self.map.selected_equipment==self:
					##print "here2"
					if self.ctrl_pressedDown:
						##print "here3"
						if event.type==pygame.MOUSEBUTTONDOWN:
							##print "here4"
							self.route.append([event.pos[0],event.pos[1]])
							self.making_route=True
							#print self.devId," has ",self.route
							##print "new route ",self.route


		elif event.button==3:
			if event.type==pygame.MOUSEBUTTONDOWN:
				if self.map.selected_equipment==self:
					self.map.selected_equipment=None

	def attach_display(self,display):
		self.display=display
		self.display.blit(self.img,self.position)

	def update_on_display(self):
		if self.manual:			
			self.position[0]+=self.speed*np.cos((180-self.angle)*(np.pi/180))
			self.position[1]+=self.speed*np.sin((180-self.angle)*(np.pi/180))
		else:
			##print "h1",len(self.route), self.route_counter,int((self.route_counter)*self.speed) ,self.speed
			if len(self.route)>0 and int((self.route_counter)+self.speed) < len(self.route) and self.speed!=0:
				##print "h2",self.route[self.route_counter]
				self.position[0]=self.route[int((self.route_counter)+self.speed)][0]
				self.position[1]=self.route[int((self.route_counter)+self.speed)][1]
				self.route_counter+=1
				self.angle= np.arctan2((self.position[1] - self.startPosition[1]),(self.position[0] - self.startPosition[0]))*(180/np.pi)
			elif int((self.route_counter)+self.speed) >= len(self.route):
				self.speed=0
				##print "h3"


		#print "self.attachedBeacons",self.attachedBeacons
		i=1
		for beacon in self.attachedBeacons:
			#print "here"
			beacon.update_pixel_location([self.position[0],self.position[1]])
			beacon.blitPosition=[self.position[0]+i*20,self.position[1]]
			i+=1
			#self.display.blit(beacon.img,beacon.position)
		rotated_img=pygame.transform.rotate(self.img, self.angle)
		if self.map.selected_equipment==self:
			#print "here",distanceBetweenPoints(self.map.keyLocations[0],self.position)*(self.mapRatio/1000)
			pygame.draw.circle(rotated_img, (255,0,0), (4,4), 2)
			red=False
			for keyLocation in self.map.keyLocations:
				if distanceBtwCoordinates(self.pixles_to_latlong(keyLocation),self.pixles_to_latlong(self.position)) > 20:
					red=False
				else:
					red=True
					break
			if not red:#40 is here threshold to change reference location
				pygame.draw.rect(rotated_img, (0,255,0), (0, 0, 47, 47), 2)
				#print "1"
			else:
				#print "2"
				pygame.draw.rect(rotated_img, (255,0,0), (0, 0, 47, 47), 2)
		#print type(self.display),type(self.position)

		pygame.draw.circle(self.display, (255,0,0), (int(self.position[0]),int(self.position[1])), 2)
		self.display.blit(rotated_img,self.position)
		self.rect.x,self.rect.y,self.rect.w,self.rect.h=self.position[0],self.position[1],rotated_img.get_width(),rotated_img.get_height()




	def rotate_clockwise(self):
		self.angle+=self.rotate_step

	def rotate_anticlockwise(self):
		self.angle-=self.rotate_step

	def start_stop(self):
		if self.running:
			self.speed=0;
			self.running=False
		else:
			self.speed=self.speed_step
			self.running=True

	def speed_up(self):
		self.speed+=self.speed_step

	def slow_down(self):
		self.speed-=self.speed_step

	def init(self):
		if self.generateData:
			global event_count
			self.one_second_event_type = pygame.USEREVENT + event_count
			event_count+=1
			pygame.time.set_timer(self.one_second_event_type, 1000)
			self.fifteen_second_event_type = pygame.USEREVENT + 2
			event_count+=1
			pygame.time.set_timer(self.fifteen_second_event_type, 15000)
			
			self.current_datetime=datetime.now()
			self.file_name="unprocessed/BC{}_{}-{}".format("".join(self.devId.split("_")),"".join(self.current_datetime.strftime('%y-%m-%d').split('-')),"".join(self.current_datetime.strftime('%H-%M-%S').split('-')))+".TXL"
			self.file=open(self.file_name,'w')
			self.file.write("message=DATA;devID={};fileName=5af18ace.TXL;token=KEY;temperature=61;data=\r\n".format(self.devId))

	def process_events(self,event):
		if event.type==self.one_second_event_type:
			self.one_second_event()
		if event.type==self.fifteen_second_event_type:
			self.fifteen_second_event();
		if event.type==pygame.KEYDOWN :
			if not self.map.selected_equipment==self:
				#print "if not self.map.selected_equipment==self",event.key==W_KEY ,self.shift_pressed ,self.ctrl_pressedDown

				if event.key==P_KEY and len(self.route) > 0 :
					#print "event.key==W_KEY and self.shift_pressed and self.ctrl_pressedDown"
					#self.route_counter=int(self.route_counter+self.speed)
					self.speed+=1
					#self.speed= self.speed+(self.speed* 0.1) if self.speed !=0 else 0.5
					if not self.route_resampled :
						if self.modify_saved_route:
							with open(self.devId+".txt","w") as f:
								f.write(str(self.route))
							self.modify_saved_route=False
						self.route=list(resample_route(np.array(self.route),2000))
						self.route_resampled=True
						print "resample_route ",self.route

				elif event.key==S_KEY and self.shift_pressed and self.ctrl_pressedDown:
					self.speed-=0.5
			else:
				if event.key==RIGHT_ARROW_KEY and self.manual:
					self.rotate_clockwise()
				elif event.key==LEFT_ARROW_KEY and self.manual:
					self.rotate_anticlockwise()		
				elif event.key==W_KEY :
					if self.manual:
						self.speed_up()
					else:
						if self.ctrl_pressedDown:
							#self.route_counter=int(self.route_counter+self.speed)
							self.speed+=1
							#self.speed= self.speed+(self.speed* 0.1) if self.speed !=0 else 0.5
							if not self.route_resampled:
								self.route=list(resample_route(np.array(self.route),1000))
								self.route_resampled=True
								#print "route_resampled"
							#rint "resample_route : ",self.route

				elif event.key==S_KEY:
					if self.manual:
						self.slow_down()
					else:
						if  self.ctrl_pressedDown :
							self.speed-=0.5
				elif event.key==SPACE_BAR_KEY and self.manual :
					self.start_stop()
				elif event.key==CTRL_KEY :
					#print "self.ctrl_pressedDown=True"
					self.ctrl_pressedDown=True
				elif event.key==SHIFT_KEY :
					self.shift_pressed=True
					#print "self.shift_pressed=True"
					##print "ctrl_down"
				elif event.key==A_KEY and self.ctrl_pressedDown==True:
					self.manual=False
				elif event.key==M_KEY and self.ctrl_pressedDown==True:
					self.manual=True
				elif event.key==DELETE_KEY and self.ctrl_pressedDown==True:
					self.route=[]
					self.route_counter=0
					self.route_resampled=False
				elif event.key==L_KEY and self.ctrl_pressedDown==True:
					self.route=eval(open(self.devId+".txt","r").read())
					print "self.route",self.route
				elif event.key==K_KEY and self.ctrl_pressedDown==True:
					self.modify_saved_route=True
				elif event.key==NUMERIC_PLUS_KEY and self.ctrl_pressedDown==True:
					nearest_beacon=self.map.getNearestBeacon(self)
					if nearest_beacon is not None:
						self.attachedBeacons.append(nearest_beacon)
				elif event.key==NUMERIC_MINUS_KEY and self.ctrl_pressedDown==True:
					if len(self.attachedBeacons) >0:
						self.attachedBeacons[-1].blitPosition=[self.attachedBeacons[-1].position[0],self.attachedBeacons[-1].position[1]]
						self.attachedBeacons=self.attachedBeacons[:-1]
						print "self.attachedBeacons",self.attachedBeacons



		if event.type==pygame.KEYUP and self.map.selected_equipment==self:
			if event.key==CTRL_KEY:
				self.ctrl_pressedDown=False
				#print "self.ctrl_pressedDown=False"
				self.making_route=False
			elif event.key==SHIFT_KEY :
				self.shift_pressed=False
				#print "self.shift_pressed=False"
				##print "ctrl_up"
		if event.type == pygame.MOUSEBUTTONDOWN or event.type==pygame.MOUSEBUTTONUP or event.type==pygame.MOUSEMOTION:
			self.mouse_event(event)

	def one_second_event(self):
		self.lat2, self.lon2 = self.pixles_to_latlong(self.position) #destination.latitude, destination.longitude
		#print self.devId,self.lat2,self.lon2,self.position
		self.current_datetime=datetime.now()
		data=None
		temp_list=list(set(self.nearBeaconsList).union(self.attachedBeacons))
		#print "temp_list",temp_list
		for beacon in temp_list:
			data=beacon.getData(self)
			if data is not None:
				self.file.write(data)
				data=None
		# for beacon in self.attachedBeacons:
		# 	if beacon not in self.nearBeaconsList:
		# 		data=beacon.getData(self)
		# 		if data is not None:
		# 			self.file.write(data)
		# 			data=None
		#print self.devId,self.lat2,self.lon2
		self.file.write("GPS,{},1,{},{},{},{},{},{},,,,,,,\r\n".format(
		self.current_datetime.strftime('%d-%m-%Y %H:%M:%S'),self.current_datetime.strftime('%H:%M:%S'),self.lat2,self.lon2,100,(self.speed*self.mapRatio),3754))
		if self.fifteen_second_done:
			self.file.close()
			#print "open(self.file_name,"r").read()",open(self.file_name,"r").read()
			ret=os.popen("mv "+self.file_name+" "+self.pro_dir).read()
			##print self.devId,ret
			self.current_datetime=datetime.now()
			self.file_name="unprocessed/BC{}_{}-{}".format("".join(self.devId.split("_")),"".join(self.current_datetime.strftime('%y-%m-%d').split('-')),"".join(self.current_datetime.strftime('%H-%M-%S').split('-')))+".TXL"
			self.file=open(self.file_name,'w')
			self.file.write("message=DATA;devID={};fileName=5af18ace.TXL;token=KEY;temperature=61;data=\r\n".format(self.devId))
			self.fifteen_second_done=False


	def fifteen_second_event(self):
		self.fifteen_second_done=True

	def pixles_to_latlong(self,position):
		self.referenceMapping,self.referenceOrigin=smallest_reference(self.map.point_mappings,self.position)
		d=np.sqrt(np.power(position[0] - self.referenceMapping[0],2) + np.power(position[1] - self.referenceMapping[1],2))/(self.mapRatio)
		###print "d",d,self.position[0],self.startPosition[0],(self.position[0] - self.startPosition[0])
		if position[0] == self.referenceMapping[0]:
			b=0
		else:
			b=90 + np.arctan2((position[1] - self.referenceMapping[1]),(position[0] - self.referenceMapping[0]))*(180/np.pi) 
		###print "b",b,90-b
		destination = VincentyDistance(kilometers=d).destination(self.referenceOrigin, b)
		#self.lat2, self.lon2 = 
		return destination.latitude, destination.longitude
		##print self.devId,self.lat2,self.lon2
		#self.current_datetime=datetime.now()
		#self.file.write("GPS,{},1,{},{},{},{},{},{},,,,,,,\r\n".format(
		#self.current_datetime.strftime('%d-%m-%Y %H:%M:%S'),self.current_datetime.strftime('%H:%M:%S'),self.lat2,self.lon2,100,(self.speed*self.mapRatio)/1000.0,3754))		


class NormalBeacon:
	def __init__(self,img_path,uuid,macAddr,position=[306,168],mapObject=None):
		self.startPosition=[position[0],position[1]]
		self.position=position
		self.gpsPostion=None
		self.uuid=uuid
		self.macAddr=macAddr
		self.img_path=img_path
		self.img=pygame.image.load(self.img_path)
		self.txPwr=-59
		if mapObject is not None:
			self.map=mapObject
			self.map.beaconList.append(self)
		else:
			self.map=None
		self.blitPosition=[position[0],position[1]]

	def update_pixel_location(self,new_pixel_location):
		print "update_pixel_location"
		self.position=new_pixel_location

	def get_position(self):
		return self.position

	def isNear(self,pixel_location,near_threshold_in_pixles):
		#print self.uuid,"isNear",distanceBetweenPoints(self.position,pixel_location),near_threshold_in_pixles
		if distanceBetweenPoints(self.position,pixel_location) < near_threshold_in_pixles:
			return True
		else:
			return False

	def attach_map(self,mapObject):
		if mapObject is None:
			print "map is None"
		else:
			self.map=mapObject
			self.map.beaconList.append(self)

	def getRSSI(self,device_loc,beacon_loc):
		distance=distanceBtwCoordinates(device_loc,beacon_loc)
		rssi=int(self.txPwr - 20*np.log10(distance))
		return rssi

	def getData(self,equipmentObject):
		s="BT,{},{},{},,0000,00,{},-59,1,{},{},{},{},{},{},{},{},{},{}\r\n".format(equipmentObject.current_datetime.strftime('%d-%m-%Y %H:%M:%S')
			,self.macAddr,self.uuid,self.getRSSI([equipmentObject.lat2,equipmentObject.lon2],equipmentObject.pixles_to_latlong(self.position)),equipmentObject.current_datetime.strftime('%H:%M:%S'),equipmentObject.lat2,equipmentObject.lon2
			,100,(equipmentObject.speed*equipmentObject.mapRatio),3754,-1,-1,-1,-1 )
		#print s
		return s

class MotionBeacon:
	def __init__(self,img_path,uuid,macAddr,position=[306,168],mapObject=None):
		self.startPosition=[position[0],position[1]]
		self.position=position
		self.gpsPostion=None
		self.uuid=uuid
		self.macAddr=macAddr
		self.img_path=img_path
		self.img=pygame.image.load(self.img_path)
		self.txPwr=-59
		if mapObject is not None:
			self.map=mapObject
			self.map.beaconList.append(self)
		else:
			self.map=None
		self.motion=False
		self.motion_readings=[0,0,0]
		self.blitPosition=[position[0],position[1]]

	def update_pixel_location(self,new_pixel_location):
		if ( ( (self.position[0] - new_pixel_location[0]) != 0 )) : 
			self.motion==True
			self.motion_readings[0]= 0 if self.motion_readings[0]!=0 else 255
		else:
			self.motion=False
		if ( (self.position[1] - new_pixel_location[1]) != 0):
			self.motion==True
			self.motion_readings[1]= 0 if self.motion_readings[1]!=0 else 255
		else:
			self.motion=False or self.motion
		self.position=new_pixel_location

	def get_position(self):
		return self.position

	def isNear(self,pixel_location,near_threshold_in_pixles):
		#Eprint self.devId,"isNear",distanceBetweenPoints(self.position,pixel_location),near_threshold_in_pixles
		if distanceBetweenPoints(self.position,pixel_location) < near_threshold_in_pixles:
			return True
		else:
			return False

	def attach_map(self,mapObject):
		if mapObject is None:
			print "map is None"
		else:
			self.map=mapObject
			self.map.beaconList.append(self)

	def getMotionReadings(self):
		return self.motion_readings


	def getRSSI(self,device_loc,beacon_loc):
		distance=distanceBtwCoordinates(device_loc,beacon_loc)
		rssi=int(self.txPwr - 20*np.log10(distance))
		return rssi

	def getData(self,equipmentObject):
		motion_readings=self.getMotionReadings()
		s="BT,{},{},{},,0000,00,{},-59,1,{},{},{},{},{},{},{},{},{},{}\r\n".format(equipmentObject.current_datetime.strftime('%d-%m-%Y %H:%M:%S')
			,self.macAddr,self.uuid,self.getRSSI([equipmentObject.lat2,equipmentObject.lon2],equipmentObject.pixles_to_latlong(self.position)),equipmentObject.current_datetime.strftime('%H:%M:%S'),equipmentObject.lat2,equipmentObject.lon2
			,100,(equipmentObject.speed*equipmentObject.mapRatio),3754,motion_readings[0],motion_readings[1],-1,motion_readings[2])
		#
		#print s
		return s

class Map:
	def __init__(self,display,name="DefaultMap",point_mappings=None,mapImg=None,keyLocations=None):
		self.display=display
		self.name=name
		self.onMapEquipmentList=[]
		self.beaconList=[]
		self.mapImg=mapImg
		self.selected_equipment=None
		self.keyLocations=keyLocations
		if point_mappings is not None and len(point_mappings["1"]) > 2 and len(point_mappings["2"]) > 2 :
			self.point_mappings=point_mappings			
			self.point_mapping1=point_mappings["1"]
			self.point_mapping2=point_mappings["2"]
			self.mapRatio=distanceBetweenPoints(self.point_mapping1[:2],self.point_mapping2[:2])/(distanceBtwCoordinates(self.point_mapping1[2:],self.point_mapping2[2:])/1000.0)
		else:
			self.point_mapping1=None
			self.point_mapping2=None
			self.mapRatio=10000
			self.point_mappings=None

	def add_equipment(self,equipment):
		self.onMapEquipmentList.append(equipment)
		equipment.map=self
		if self.point_mapping1 is not None:
			equipment.referenceMapping=self.point_mapping1
			equipment.mapRatio=self.mapRatio
			equipment.referenceOrigin=geopy.Point(self.point_mapping1[2],self.point_mapping1[3])

	def update_selected(self,equipment):
		self.selected_equipment=equipment

	def updateMap(self):
		for equipment in self.onMapEquipmentList:
			temp_beacon_list=[]
			#print "here",equipment.devId,self.beaconList
			for beacon in self.beaconList:
				#print "(self.mapRatio*10.0)/1000.0",(self.mapRatio*20.0)/1000.0,equipment.position
				if beacon.isNear(equipment.position,(self.mapRatio*20.0)/1000.0):
					temp_beacon_list.append(beacon)
					if beacon in equipment.attachedBeacons:
						pygame.draw.line(self.display,(0,0,255),equipment.position,[beacon.blitPosition[0],beacon.blitPosition[1]],5)
					else:
						pygame.draw.line(self.display,(255,0,0),equipment.position,[beacon.blitPosition[0],beacon.blitPosition[1]],5)
			equipment.nearBeaconsList=temp_beacon_list
			
	def addBeacons(self,beacon):
		self.beaconList.append(beacon)
		#fprint "self.beaconList",self.beaconList
			#self.display.blit(line,)

	def getNearestBeacon(self,equipmentObject):
		possible_beacons=[beacon for beacon in equipmentObject.nearBeaconsList if beacon not in equipmentObject.attachedBeacons]
		if len(possible_beacons)>0:
			rssi_list=[beacon.getRSSI([equipmentObject.lat2,equipmentObject.lon2],equipmentObject.pixles_to_latlong(beacon.position)) for beacon in possible_beacons]
			nearest_idx=rssi_list.index(min(rssi_list))
			return possible_beacons[nearest_idx]
		else:
			return None
